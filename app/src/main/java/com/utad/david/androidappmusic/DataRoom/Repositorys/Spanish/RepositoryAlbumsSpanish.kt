package com.utad.david.androidappmusic.DataRoom.Repositorys.Spanish

import android.app.Application
import android.arch.lifecycle.LiveData
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish
import com.utad.david.androidappmusic.DataRoom.Daos.Spanish.DaoAlbumsSpanish
import com.utad.david.androidappmusic.Models.English.AlbumEnglish
import com.utad.david.androidappmusic.Models.Spanish.AlbumSpanish

class RepositoryAlbumsSpanish private constructor(application: Application) {

    /*
Creamos una instancia del dao y un livedata para poder observar los datos en diferentes partes de nuestra aplicacion
*/

    private var mDaoAlbumsSpanish: DaoAlbumsSpanish? = null
    private var mAlbums: LiveData<List<AlbumSpanish>>? = null


    companion object {
        @Volatile
        private var INSTANCE: RepositoryAlbumsSpanish? = null

        fun getInstance(context: Application): RepositoryAlbumsSpanish =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: RepositoryAlbumsSpanish(context).also { INSTANCE = it }
            }
    }

    init {
        BDMusicAppEnglish.getInstance(application).also {
            mDaoAlbumsSpanish = it.albumDaoSpanish()
            mAlbums = mDaoAlbumsSpanish?.getAllAlbums()
        }
    }


    fun getAllAlbums(): LiveData<List<AlbumSpanish>>? {
        return mAlbums
    }

}