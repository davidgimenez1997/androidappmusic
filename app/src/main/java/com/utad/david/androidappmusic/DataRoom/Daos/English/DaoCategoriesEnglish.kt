package com.utad.david.androidappmusic.DataRoom.Daos.English

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.utad.david.androidappmusic.Models.English.CategoriesEnglish

@Dao
interface DaoCategoriesEnglish {

    /*
    Sentencias sql de nuestra base de datos
     */

    @Insert
    fun insertAll(listCategory:List<CategoriesEnglish>)

    @Query("SELECT * from categoriesEnglish ")
    fun getAllCategories(): LiveData<List<CategoriesEnglish>>

    @Query("DELETE from categoriesEnglish")
    fun deleteAll()
}