package com.utad.david.androidappmusic.Models.Spanish

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

/*
Se importa Serializable para poder pasar un objeto de esta clase entre pantallas
 */

@Entity(tableName = "albumsSpanish")
data class AlbumSpanish(
    var name: String,
    var image: String,
    var description: String,
    var artist: String,
    var genre: String,
    var songEnglishes: List<SongSpanish>,
    var price:Double,
    var isFavorite:Boolean


):Serializable {
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0

}