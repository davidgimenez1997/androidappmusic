package com.utad.david.androidappmusic.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.AlbumEnglish
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.AlbumSpanish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish
import com.utad.david.androidappmusic.R
import kotlinx.android.synthetic.main.item_album_song.view.*

class AdapterAlbumSong() : RecyclerView.Adapter<AdapterAlbumSong.ViewHolderAlbumSong>(){

    interface onItemClickedAdapter{
        fun clickItemSongEnglish(songEnglish: SongEnglish)
        fun clickItemAlbumEnglish(albumEnglish: AlbumEnglish)

        fun clickItemSongSpanish(songSpanish: SongSpanish)
        fun clickItemAlbumSpanish(albumSpanish: AlbumSpanish)
    }

    private var mCallback : onItemClickedAdapter? = null

    fun setCallback(callback: onItemClickedAdapter){
        mCallback = callback
    }

    companion object {
        private var option:Int = 0;
        private var albumEnglishList: List<AlbumEnglish>? = null;
        private var albumSpanishList: List<AlbumSpanish>? = null;
        private lateinit var context : Context

        private var songEnglishList: List<SongEnglish>? = null;
        private var songSpanishList: List<SongSpanish>? = null;


        /*
Diferentes instancias donde las pasas el List dependiendo del idioma con la info
 */
        fun newInstanceAlbumEnglish(option: Int, albumEnglishList: List<AlbumEnglish>?,context: Context): AdapterAlbumSong {
            this.option = option;
            this.albumEnglishList = albumEnglishList;
            this.context = context
            return AdapterAlbumSong()
        }

        fun newInstanceAlbumSpanish(option: Int, albumSpanishList: List<AlbumSpanish>?,context: Context): AdapterAlbumSong {
            this.option = option;
            this.albumSpanishList = albumSpanishList;
            this.context = context
            return AdapterAlbumSong()
        }

        fun newInstanceSongEnglish(option: Int, songEnglishList: List<SongEnglish>?,context: Context): AdapterAlbumSong {
            this.option = option;
            this.songEnglishList = songEnglishList;
            this.context = context
            return AdapterAlbumSong()
        }

        fun newInstanceSongSpanish(option: Int, songSpanishList: List<SongSpanish>?,context: Context): AdapterAlbumSong{
            this.option = option;
            this.songSpanishList = songSpanishList
            this.context = context
            return AdapterAlbumSong()
        }

        /*
        Metodos setters
         */
        fun setDataSongEnglish(songEnglishList: List<SongEnglish>?){
            this.songEnglishList = songEnglishList
        }

        fun setDataAlbumEnglish(albumEnglishList: List<AlbumEnglish>?){
            this.albumEnglishList = albumEnglishList
        }

        fun setDataSongSpanish(songSpanishList: List<SongSpanish>?){
            this.songSpanishList = songSpanishList
        }

        fun setDataAlbumSpanish(albumSpanishList: List<AlbumSpanish>?){
            this.albumSpanishList = albumSpanishList
        }

    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderAlbumSong {
        val rootView = LayoutInflater.from(p0.context).inflate(R.layout.item_album_song, p0, false)
        return ViewHolderAlbumSong(rootView)
    }

    /*
      Option = 0 -> Has pinchado en Song
      Option = 1 -> Has pinchado en Album
 */

    override fun getItemCount(): Int {
        if(LocaleHelper().getLanguage(context).equals("en")){
            when(option){
                0->{
                    songEnglishList?.let {
                       return it.size
                    }
                }
                1->{
                    albumEnglishList?.let {
                       return it.size
                    }
                }
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            when(option){
                0-> songSpanishList?.let {
                    return it.size
                }
                1-> albumSpanishList?.let {
                    return it.size
                }
            }
        }

        return 0
    }

    override fun onBindViewHolder(myViewHolder: ViewHolderAlbumSong, position: Int) {
        when(option){
            0->{
                viewHolderSong(myViewHolder, position, context)
            }
            1->{
                viewHolderAlbum(myViewHolder, position, context)
            }
        }
    }


    /*
    Configura la info de la celda dependiendo del idioma y el producto. Ponemos un onClick a la celda
     */
    private fun viewHolderSong(myViewHolder: ViewHolderAlbumSong, position: Int,context: Context){
        if(LocaleHelper().getLanguage(context).equals("en")){
            songEnglishList.let {
                val item = it!!.get(position)
                myViewHolder.setValueSongEnglish(item)
                myViewHolder.itemView.setOnClickListener {
                    mCallback?.let {
                        it.clickItemSongEnglish(item)
                    }

                }
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")) {
            songSpanishList.let {
                val item = it!!.get(position)
                myViewHolder.setValueSongSpanish(item)
                myViewHolder.itemView.setOnClickListener {
                    mCallback?.let {
                        it.clickItemSongSpanish(item)
                    }

                }
            }
        }
    }

    private fun viewHolderAlbum(myViewHolder: ViewHolderAlbumSong, position: Int,context: Context){
        if(LocaleHelper().getLanguage(context).equals("en")){
            albumEnglishList.let {
                val item = it?.get(position)
                item?.let {album->
                    myViewHolder.setValueAlbumEnglish(album)
                    myViewHolder.itemView.setOnClickListener {
                        mCallback?.let {
                            it.clickItemAlbumEnglish(album)
                        }

                    }
                }

            }
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            albumSpanishList.let {
                val item = it?.get(position)
                item?.let { albumSpanish ->
                    myViewHolder.setValueAlbumSpanish(albumSpanish)
                    myViewHolder.itemView.setOnClickListener {
                        mCallback?.let {
                            it.clickItemAlbumSpanish(albumSpanish)
                        }

                    }
                }
            }
        }
    }

    inner class ViewHolderAlbumSong(val view: View): RecyclerView.ViewHolder(view) {
        var name: TextView = itemView.texViewName
        var photo: ImageView = itemView.imageView
        var info : TextView = itemView.textViewInfo

        /*
       Ponemos la información del item en la celda
        */

        fun setValueAlbumEnglish(item: AlbumEnglish) {
            name.setText(item.name)
            Glide.with(itemView).load(item.image).into(photo)
            info.text = item?.genre
        }

        fun setValueAlbumSpanish(item: AlbumSpanish) {
            name.setText(item.name)
            Glide.with(itemView).load(item.image).into(photo)
            info.text = item?.genre
        }

        fun setValueSongEnglish(item: SongEnglish){
            name.setText(item.name)
            info.setText(item.duration.toString())
            Glide.with(itemView).load(item.image).into(photo)
        }

        fun setValueSongSpanish(item: SongSpanish){
            name.setText(item.name)
            info.setText(item.duration.toString())
            Glide.with(itemView).load(item.image).into(photo)
        }
    }
}