package com.utad.david.androidappmusic.DataRoom.Daos.Spanish

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish

@Dao
interface DaoSongsSpanish{

    /*
    Sentencias sql de nuestra base de datos
     */

    @Insert
    fun insertAll(listSongEnglish:List<SongSpanish>)

    @Query("SELECT * from SONGSSPANISH ")
    fun getAllSongs(): LiveData<List<SongSpanish>>

    @Query("Delete from SONGSSPANISH")
    fun deleteAll()
}