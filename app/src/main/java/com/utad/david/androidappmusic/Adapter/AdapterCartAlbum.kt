package com.utad.david.androidappmusic.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.AlbumEnglish
import com.utad.david.androidappmusic.Models.Spanish.AlbumSpanish
import com.utad.david.androidappmusic.R
import kotlinx.android.synthetic.main.item_card.view.*

class AdapterCartAlbum() : RecyclerView.Adapter<AdapterCartAlbum.ViewHolderAlbum>(){

    companion object {

        private var albumEnglishList: List<AlbumEnglish> = listOf()
        private var albumSpanishList: List<AlbumSpanish> = listOf()
        private lateinit var context : Context

        /*
        Diferentes instancias donde las pasas el List dependiendo del idioma con la info
         */

        fun newInstanceAlbumEnglish(albumEnglishList: List<AlbumEnglish>, context: Context): AdapterCartAlbum {
            this.albumEnglishList = albumEnglishList;
            this.context = context
            return AdapterCartAlbum()
        }

        fun newInstanceAlbumSpanish(albumSpanishList: List<AlbumSpanish>, context: Context): AdapterCartAlbum {
            this.albumSpanishList = albumSpanishList;
            this.context = context
            return AdapterCartAlbum()
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderAlbum {
        val rootView = LayoutInflater.from(p0.context).inflate(R.layout.item_card, p0, false)
        return ViewHolderAlbum(rootView)
    }

    override fun getItemCount(): Int {
        if(LocaleHelper().getLanguage(context).equals("en")){
            albumEnglishList?.let {
                return it.size
            }
        } else if(LocaleHelper().getLanguage(context).equals("es")){
            albumSpanishList?.let {
                return it.size
            }
        }

        return 0
    }

    override fun onBindViewHolder(myViewHolder: ViewHolderAlbum, position: Int) {
        viewHolderAlbum(myViewHolder, position, context)
    }

    private fun viewHolderAlbum(myViewHolder: ViewHolderAlbum, position: Int,context: Context){
        if(LocaleHelper().getLanguage(context).equals("en")){
            albumEnglishList.let {
                val item = it?.get(position)
                myViewHolder.setValueAlbumEnglish(item)
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            albumSpanishList.let {
                val item = it?.get(position)
                myViewHolder.setValueAlbumSpanish(item)
            }
        }
    }

    inner class ViewHolderAlbum(val view: View): RecyclerView.ViewHolder(view) {
        var title: TextView = itemView.textViewTitlecard
        var photo: ImageView = itemView.imagenviewCard
        var genre : TextView = itemView.textViewGenreCard
        var price : TextView = itemView.textViewPrice


        /*
        Ponemos la información del item en la celda
         */
        fun setValueAlbumEnglish(item: AlbumEnglish) {
            title.setText(item.name)
            Glide.with(itemView).load(item.image).into(photo)
            price.text = item.price.toString()
            genre.text = item?.genre
        }

        fun setValueAlbumSpanish(item: AlbumSpanish) {
            title.setText(item.name)
            Glide.with(itemView).load(item.image).into(photo)
            price.text = item.price.toString()
            genre.text = item?.genre
        }

    }
}