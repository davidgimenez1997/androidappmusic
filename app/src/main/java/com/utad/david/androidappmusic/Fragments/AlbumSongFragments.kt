package com.utad.david.androidappmusic.Fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.utad.david.androidappmusic.DialogFragments.SongDetails
import com.utad.david.androidappmusic.Adapter.AdapterAlbumSong
import com.utad.david.androidappmusic.Adapter.AdapterCategories
import com.utad.david.androidappmusic.DialogFragments.AlbumDetails
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.AlbumEnglish
import com.utad.david.androidappmusic.Models.English.CategoriesEnglish
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.AlbumSpanish
import com.utad.david.androidappmusic.Models.Spanish.CategoriesSpanish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish

import com.utad.david.androidappmusic.R

class AlbumSongFragments : Fragment(), AdapterAlbumSong.onItemClickedAdapter, AdapterCategories.onItemClickedAdapter{


    private lateinit var myRecycleView: RecyclerView
    private lateinit var mRecycleViewCategory: RecyclerView
    private lateinit var myAdapter: RecyclerView.Adapter<AdapterAlbumSong.ViewHolderAlbumSong>
    private lateinit var myAdapterCategories: RecyclerView.Adapter<AdapterCategories.CategoriesViewHolder>
    private lateinit var myLayoutManger: RecyclerView.LayoutManager
    private lateinit var mStaggeredGridLayoutManager: StaggeredGridLayoutManager

    /*
    Listas de song,albums en ambos idiomas que empleamos en la categoria All/Todos
     */

    private var listAlbumEnglishAll: List<AlbumEnglish>? = null
    private var listSongEnglishAll: List<SongEnglish>? = null
    private var listAlbumSpanishAll: List<AlbumSpanish>? = null
    private var listSongsSpanishAll: List<SongSpanish>? = null

    /*
    Listas generales de la clase
     */

    private var listCategoriesEnglish: List<CategoriesEnglish>? = null
    private var listCategoriesSpanish: List<CategoriesSpanish>? = null
    private var listSongEnglish: List<SongEnglish>? = null
    private var listSongSpanish: List<SongSpanish>? = null
    private var listAlbumEnglish: List<AlbumEnglish>? = null
    private var listAlbumSpanish: List<AlbumSpanish>? = null


    /*
        Option = 0 -> Has pinchado en Song
        Option = 1 -> Has pinchado en Album
   */

    companion object {
        private var option: Int = 0;

        fun newInstanceSong(option: Int): AlbumSongFragments {
            this.option = option;
            return AlbumSongFragments()
        }

        fun newInstanceAlbum(option: Int): AlbumSongFragments {
            this.option = option;
            return AlbumSongFragments()
        }
    }

    /*
    Metodos setters
     */

    fun setListCategorieSpanish(list: List<CategoriesSpanish>?){
        this.listCategoriesSpanish = list
    }

    fun setListCategorieEnglish(list: List<CategoriesEnglish>?){
        this.listCategoriesEnglish = list
    }

    fun setListSongSpanish(list: List<SongSpanish>?){
        this.listSongSpanish = list
    }

    fun setListSongEnglish(list: List<SongEnglish>?){
        this.listSongEnglish = list
    }

    fun setListAlbumSpanish(list: List<AlbumSpanish>?){
        this.listAlbumSpanish = list
    }

    fun setListAlbumEnglish(list: List<AlbumEnglish>?){
        this.listAlbumEnglish = list
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_album_song_fragments, container, false)

        configRecycleView(view)

        if (LocaleHelper().getLanguage(view.context).equals("en")) {

            loadDataCategoriesEnglish(view.context)

            when (option) {
                0 -> {
                    loadDataSongEnglish(view.context)
                }
                1 -> {
                    loadDataAlbumEnglish(view.context)
                }
            }

        } else if (LocaleHelper().getLanguage(view.context).equals("es")) {

            loadDataCategoriesSpanish(view.context)

            when (option) {
                0 -> {
                    loadDataSongSpanish(view.context)
                }
                1 -> {
                    loadDataAlbumSpanish(view.context)
                }
            }
        }

        return view
    }

    /*
    Se configuran los recycleview de categorias o albums/canciones
     */

    private fun configRecycleView(view: View){

        myRecycleView = view.findViewById(R.id.recycleViewAlbumSong)
        mRecycleViewCategory = view.findViewById(R.id.recycleViewCategories)

        //Canciones/albumes

        myRecycleView.setHasFixedSize(true)
        myLayoutManger = GridLayoutManager(view.context, 4)
        myRecycleView.layoutManager = myLayoutManger

        //Categorias

        mRecycleViewCategory.setHasFixedSize(true)
        mStaggeredGridLayoutManager = StaggeredGridLayoutManager(1, LinearLayout.HORIZONTAL)
        mRecycleViewCategory.setLayoutManager(mStaggeredGridLayoutManager)

    }

    /*
    Dependiendo de la "option" seleccionada se iniciara el adapter de categorias correspondiente
     */

    private fun loadDataCategoriesEnglish(context: Context){
        when (option) {
            0 -> {
                myAdapterCategories = AdapterCategories.newInstanceCategoriesEnglish(listCategoriesEnglish,context).apply {
                    setCallback(this@AlbumSongFragments)
                    setType("song")
                }
                mRecycleViewCategory.adapter = myAdapterCategories

            }
            1 -> {
                myAdapterCategories = AdapterCategories.newInstanceCategoriesEnglish(listCategoriesEnglish,context).apply {
                    setCallback(this@AlbumSongFragments)
                    setType("album")
                }
                mRecycleViewCategory.adapter = myAdapterCategories
                myAdapterCategories.notifyDataSetChanged()

            }
        }
    }

    private fun loadDataCategoriesSpanish(context: Context){
        when (option) {
            0 -> {
                myAdapterCategories = AdapterCategories.newInstanceCategoriesSpanish(listCategoriesSpanish,context).apply {
                    setCallback(this@AlbumSongFragments)
                    setType("song")
                }
                mRecycleViewCategory.adapter = myAdapterCategories

            }
            1 -> {
                myAdapterCategories = AdapterCategories.newInstanceCategoriesSpanish(listCategoriesSpanish,context).apply {
                    setCallback(this@AlbumSongFragments)
                    setType("album")
                }
                mRecycleViewCategory.adapter = myAdapterCategories

            }
        }
    }

    /*
    Creamos los adapters de song y de albums en ambos idiomas.
     */

    private fun loadDataSongEnglish(context: Context){
        listSongEnglishAll = listSongEnglish
        myAdapter = AdapterAlbumSong.newInstanceSongEnglish(option, listSongEnglish,context).apply {
            setCallback(this@AlbumSongFragments)
        }
        myRecycleView.adapter = myAdapter
    }

    private fun loadDataAlbumEnglish(context: Context){
        listAlbumEnglishAll = listAlbumEnglish
        Log.d("Pruba",""+listAlbumEnglishAll?.size)
        myAdapter = AdapterAlbumSong.newInstanceAlbumEnglish(option, listAlbumEnglish,context).apply {
            setCallback(this@AlbumSongFragments)
        }
        myRecycleView.adapter = myAdapter
    }

    private fun loadDataSongSpanish(context: Context) {
        listSongsSpanishAll = listSongSpanish
        myAdapter = AdapterAlbumSong.newInstanceSongSpanish(option, listSongSpanish,context).apply {
            setCallback(this@AlbumSongFragments)
        }
        myRecycleView.adapter = myAdapter
    }

    private fun loadDataAlbumSpanish(context: Context) {
        listAlbumSpanishAll = listAlbumSpanish
        myAdapter = AdapterAlbumSong.newInstanceAlbumSpanish(option, listAlbumSpanish,context).apply {
            setCallback(this@AlbumSongFragments)
        }
        myRecycleView.adapter = myAdapter
    }

    /*
    Callbacks de la celda para mostrar el detalle de las conciones y de los albumnes
     */

    override fun clickItemSongEnglish(songEnglish: SongEnglish) {
        Log.d("Clickkkk", songEnglish.genre)
        SongDetails.newInstanceSongEnglish(songEnglish)
            .show(activity?.supportFragmentManager,"SONGENGLISH")
    }

    override fun clickItemAlbumEnglish(albumEnglish: AlbumEnglish) {
        Log.d("Clickkkk", albumEnglish.genre)
        AlbumDetails.newInstanceAlbumEnglish(albumEnglish)
            .show(activity?.supportFragmentManager,"ALBUMENGLISH")

    }

    override fun clickItemSongSpanish(songSpanish: SongSpanish) {
        Log.d("Clickkkk", songSpanish.genre)
        SongDetails.newInstanceSongsSpanish(songSpanish)
            .show(activity?.supportFragmentManager,"SONGSPANISH")
    }

    override fun clickItemAlbumSpanish(albumSpanish: AlbumSpanish) {
        Log.d("Clickkkk", albumSpanish.genre)
        AlbumDetails.newInstanceAlbumSpanish(albumSpanish)
            .show(activity?.supportFragmentManager,"ALBUMSPANISH")

    }

    /*
        Callbacks de la celda de categorias
     */

    override fun clickCategorieEnglish(nameCategories: String, type: String) {
        if (type.equals("song")) {
            clickCategorieSongEnglish(nameCategories)
        } else if (type.equals("album")) {
            clickCategorieAlbumEnglish(nameCategories)
        }
    }

    override fun clickCategorieSpanish(nameCategories: String, type: String) {
        if (type.equals("song")) {
            clickCategorieSongSpanish(nameCategories)
        } else if (type.equals("album")) {
            clickCategorieAlbumSpanish(nameCategories)
        }
    }

    /*
    Creamos una lista auxiliar y recorremos la lista que tenemos actualmente cargada de la base de datos.
    Seteamos la lista auxiliar al adapter. En caso de que la categoria sea "All" o "Todos", se pasa la lista completa, en caso contrario se pasara la lista auxiliar
    compuesta de elementos con la categoria selecionada.
     */

    private fun clickCategorieSongEnglish(nameCategories: String){

        var listSongEnglishAuxes: List<SongEnglish>? = listOf()

        listSongEnglishAll?.forEach {
            if (listSongEnglishAuxes != null) {
                if (it.genre.equals(nameCategories)) {
                    Log.d("Clickkkk", "Cancion " + it.toString() + " con la categoria " + nameCategories)
                    listSongEnglishAuxes += listOf(it)
                }
            }
            AdapterAlbumSong.setDataSongEnglish(listSongEnglishAuxes)
            myAdapter.notifyDataSetChanged()
        }

        if (nameCategories.equals("All")) {
            AdapterAlbumSong.setDataSongEnglish(listSongEnglishAll)
            myAdapter.notifyDataSetChanged()
        }
    }

    private fun clickCategorieAlbumEnglish(nameCategories: String){

        var listAlbumEnglishAuxes: List<AlbumEnglish>? = listOf()

        listAlbumEnglishAll?.forEach {
            if (listAlbumEnglishAuxes != null) {
                if (it.genre.equals(nameCategories)) {
                    Log.d("Clickkkk", "AlbumEnglish " + it.toString() + " con la categoria " + nameCategories)
                    listAlbumEnglishAuxes += listOf(it)
                }
            }
            AdapterAlbumSong.setDataAlbumEnglish(listAlbumEnglishAuxes)
            myAdapter.notifyDataSetChanged()
        }

        if (nameCategories.equals("All")) {
            AdapterAlbumSong.setDataAlbumEnglish(listAlbumEnglishAll)
            myAdapter.notifyDataSetChanged()
        }
    }

    private fun clickCategorieSongSpanish(nameCategories: String){

        var listSongSpanishAuxes: List<SongSpanish>? = listOf()

        listSongsSpanishAll?.forEach {
            if (listSongSpanishAuxes != null) {
                if (it.genre.equals(nameCategories)) {
                    Log.d("Clickkkk", "Cancion " + it.toString() + " con la categoria " + nameCategories)
                    listSongSpanishAuxes += listOf(it)
                }
            }
            AdapterAlbumSong.setDataSongSpanish(listSongSpanishAuxes)
            myAdapter.notifyDataSetChanged()
        }

        if (nameCategories.equals("Todos")) {
            AdapterAlbumSong.setDataSongSpanish(listSongsSpanishAll)
            myAdapter.notifyDataSetChanged()
        }
    }

    private fun clickCategorieAlbumSpanish(nameCategories: String){

        var listAlbumSpanishAuxes: List<AlbumSpanish>? = listOf()

        listAlbumSpanishAll?.forEach {
            if (listAlbumSpanishAuxes != null) {
                if (it.genre.equals(nameCategories)) {
                    Log.d("Clickkkk", "AlbumEnglish " + it.toString() + " con la categoria " + nameCategories)
                    listAlbumSpanishAuxes += listOf(it)
                }
            }
            AdapterAlbumSong.setDataAlbumSpanish(listAlbumSpanishAuxes)
            myAdapter.notifyDataSetChanged()
        }

        if (nameCategories.equals("Todos")) {
            AdapterAlbumSong.setDataAlbumSpanish(listAlbumSpanishAll)
            myAdapter.notifyDataSetChanged()
        }
    }
}

