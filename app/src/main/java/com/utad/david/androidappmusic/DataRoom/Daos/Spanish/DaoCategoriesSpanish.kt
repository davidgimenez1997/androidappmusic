package com.utad.david.androidappmusic.DataRoom.Daos.Spanish

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.utad.david.androidappmusic.Models.English.CategoriesEnglish
import com.utad.david.androidappmusic.Models.Spanish.CategoriesSpanish

@Dao
interface DaoCategoriesSpanish{

    /*
    Sentencias sql de nuestra base de datos
     */

    @Insert
    fun insertAll(listCategory:List<CategoriesSpanish>)

    @Query("SELECT * from categoriesSpanish ")
    fun getAllCategories(): LiveData<List<CategoriesSpanish>>

    @Query("DELETE from categoriesSpanish")
    fun deleteAll()
}