package com.utad.david.androidappmusic.DataRoom.Repositorys.English

import android.app.Application
import android.arch.lifecycle.LiveData
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish
import com.utad.david.androidappmusic.DataRoom.Daos.English.DaoAlbumsEnglish
import com.utad.david.androidappmusic.Models.English.AlbumEnglish

class RepositoryAlbumsEnglish private constructor(application: Application) {

    /*
         Creamos una instancia del dao y un livedata para poder observar los datos en diferentes partes de nuestra aplicacion
    */

    private var mDaoAlbumsEnglish: DaoAlbumsEnglish? = null
    private var mAlbums: LiveData<List<AlbumEnglish>>? = null


    companion object {
        @Volatile
        private var INSTANCE: RepositoryAlbumsEnglish? = null

        fun getInstance(context: Application): RepositoryAlbumsEnglish =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: RepositoryAlbumsEnglish(context).also { INSTANCE = it }
            }
    }

    init {
        BDMusicAppEnglish.getInstance(application).also {
            mDaoAlbumsEnglish = it.albumDaoEnglish()
            mAlbums = mDaoAlbumsEnglish?.getAllAlbums()
        }
    }


    fun getAllAlbums(): LiveData<List<AlbumEnglish>>? {
        return mAlbums
    }

}