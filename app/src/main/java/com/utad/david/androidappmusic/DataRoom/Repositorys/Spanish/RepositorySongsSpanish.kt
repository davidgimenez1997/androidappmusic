package com.utad.david.androidappmusic.DataRoom.Repositorys.Spanish

import android.app.Application
import android.arch.lifecycle.LiveData
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish
import com.utad.david.androidappmusic.DataRoom.Daos.Spanish.DaoSongsSpanish
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish

class RepositorySongsSpanish private constructor(application: Application) {

    /*
    Creamos una instancia del dao y un livedata para poder observar los datos en diferentes partes de nuestra aplicacion
     */

    private var mDaoSongsSpanish: DaoSongsSpanish? = null
    private var mSongs: LiveData<List<SongSpanish>>? = null


    companion object {
        @Volatile
        private var INSTANCE: RepositorySongsSpanish? = null

        fun getInstance(context: Application): RepositorySongsSpanish =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: RepositorySongsSpanish(context).also { INSTANCE = it }
            }
    }

    init {
        BDMusicAppEnglish.getInstance(application).also {
            mDaoSongsSpanish = it.songDaoSpanish()
            mSongs = mDaoSongsSpanish?.getAllSongs()
        }
    }


    fun getAllSongs(): LiveData<List<SongSpanish>>? {
        return mSongs
    }

}