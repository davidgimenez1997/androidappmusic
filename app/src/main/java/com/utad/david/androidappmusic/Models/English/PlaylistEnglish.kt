package com.utad.david.androidappmusic.Models.English

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "playlistsEnglish")
class PlaylistEnglish(
    var name: String,
    var image: Int,
    var description: String,
    var artist: String,
    var songEnglishes: List<SongEnglish>
) {
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
}