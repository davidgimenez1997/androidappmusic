package com.utad.david.androidappmusic.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.AlbumEnglish
import com.utad.david.androidappmusic.Models.Spanish.AlbumSpanish
import com.utad.david.androidappmusic.R
import kotlinx.android.synthetic.main.item_favorite.view.*


class AdapterPlayListAlbum() : RecyclerView.Adapter<AdapterPlayListAlbum.ViewHolderFavorite>(){


    interface OnFragmentInteractionListener {
        fun clickCartButtonEnglishAlbum(albumEnglish: AlbumEnglish)
        fun clickCartButtonSpanishAlbum(albumSpanish: AlbumSpanish)

    }

    private var mCallback : OnFragmentInteractionListener? = null

    fun setCallback(callback: OnFragmentInteractionListener){
        mCallback = callback
    }

    companion object {

        private var mutableListAlbumEnglish: MutableList<AlbumEnglish> = mutableListOf()
        private var mutableListAlbumSpanish: MutableList<AlbumSpanish> = mutableListOf()

        private lateinit var context : Context

        /*
Diferentes instancias donde las pasas el mutableList dependiendo del idioma con la info
 */

        fun newInstanceAlbumEnglish(mutableListAlbumEnglish: MutableList<AlbumEnglish>,context: Context): AdapterPlayListAlbum {
            this.context = context
            this.mutableListAlbumEnglish = mutableListAlbumEnglish;
            return AdapterPlayListAlbum()
        }

        fun newInstanceAlbumSpanish(mutableListAlbumSpanish: MutableList<AlbumSpanish>,context: Context): AdapterPlayListAlbum {
            this.context = context
            this.mutableListAlbumSpanish = mutableListAlbumSpanish;
            return AdapterPlayListAlbum()
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderFavorite {
        val rootView = LayoutInflater.from(p0.context).inflate(R.layout.item_favorite, p0, false)
        return ViewHolderFavorite(rootView)
    }

    override fun getItemCount(): Int {
        if(LocaleHelper().getLanguage(context).equals("en")){
            mutableListAlbumEnglish?.let {
                return it.size
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            mutableListAlbumSpanish?.let {
                return it.size
            }
        }

        return 0
    }

    override fun onBindViewHolder(myViewHolder: ViewHolderFavorite, position: Int) {
        viewHolderAlbum(myViewHolder, position, context)
    }

    private fun viewHolderAlbum(myViewHolder: ViewHolderFavorite, position: Int,context: Context){
        if(LocaleHelper().getLanguage(context).equals("en")){
            mutableListAlbumEnglish?.let {
                val item = it?.get(position)
                myViewHolder.setValueAlbumEnglish(item)
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            mutableListAlbumSpanish?.let {
                val item = it?.get(position)
                myViewHolder.setValueAlbumSpanish(item)
            }
        }
    }

    inner class ViewHolderFavorite(val view: View): RecyclerView.ViewHolder(view) {
        var name: TextView = itemView.textViewtitlefavorite
        var price :TextView = itemView.textViewpricefavorite
        var photo : ImageView = itemView.imageViewFavorite
        var cart : ImageView = itemView.imageViewfavoritecard

        /*
       Ponemos la información del item en la celda
        */

        fun setValueAlbumEnglish(item: AlbumEnglish) {
            name.setText(item.name)
            price.text = item.price.toString()
            Glide.with(itemView).load(item.image).into(photo)
            cart.setOnClickListener {
                Toast.makeText(itemView.context, context.getString(R.string.info_button_Song_card) +" "+item?.name,
                    Toast.LENGTH_SHORT).show()
                mCallback?.let {listener->
                    listener.clickCartButtonEnglishAlbum(item)
                }
            }
        }

        fun setValueAlbumSpanish(item: AlbumSpanish) {
            name.setText(item.name)
            price.text = item.price.toString()
            Glide.with(itemView).load(item.image).into(photo)
            cart.setOnClickListener {
                Toast.makeText(itemView.context,context.getString(R.string.info_button_Song_card) +" "+item?.name,
                    Toast.LENGTH_SHORT).show()
                mCallback?.let {listener->
                    listener.clickCartButtonSpanishAlbum(item)
                }
            }
        }
    }
}