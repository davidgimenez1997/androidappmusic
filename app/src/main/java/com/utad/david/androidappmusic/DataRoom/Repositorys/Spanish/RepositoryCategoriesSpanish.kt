package com.utad.david.androidappmusic.DataRoom.Repositorys.Spanish

import android.app.Application
import android.arch.lifecycle.LiveData
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish
import com.utad.david.androidappmusic.DataRoom.Daos.Spanish.DaoCategoriesSpanish
import com.utad.david.androidappmusic.Models.English.CategoriesEnglish
import com.utad.david.androidappmusic.Models.Spanish.CategoriesSpanish

class RepositoryCategoriesSpanish private constructor(application: Application) {

    /*
Creamos una instancia del dao y un livedata para poder observar los datos en diferentes partes de nuestra aplicacion
*/

    private var mDaoCategoriesSpanish: DaoCategoriesSpanish? = null
    private var mCategoriesEnglish: LiveData<List<CategoriesSpanish>>? = null


    companion object {
        @Volatile
        private var INSTANCE: RepositoryCategoriesSpanish? = null

        fun getInstance(context: Application): RepositoryCategoriesSpanish =
            INSTANCE
                ?: synchronized(this) {
                    INSTANCE
                        ?: RepositoryCategoriesSpanish(context).also { INSTANCE = it }
                }
    }

    init {
        BDMusicAppEnglish.getInstance(application).also {
            mDaoCategoriesSpanish = it.categoryDaoSpanish()
            mCategoriesEnglish = mDaoCategoriesSpanish?.getAllCategories()
        }
    }


    fun getAllCategories(): LiveData<List<CategoriesSpanish>>? {
        return mCategoriesEnglish
    }

}