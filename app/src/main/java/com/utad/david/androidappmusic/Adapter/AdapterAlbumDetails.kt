package com.utad.david.androidappmusic.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.AlbumEnglish
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.AlbumSpanish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish
import com.utad.david.androidappmusic.R
import kotlinx.android.synthetic.main.item_list_song.view.*


class AdapterAlbumDetails() : RecyclerView.Adapter<AdapterAlbumDetails.ViewHolderAlbumDetails>(){

    companion object {

        private var listSongAlbumEnglishList: List<SongEnglish>? = null;
        private var listSongAlbumSpanishList: List<SongSpanish>? = null;
        private lateinit var context : Context

        /*
       Diferentes instancias donde las pasas el List dependiendo del idioma con la info
        */
        fun newInstanceAlbumEnglish(albumEnglishList: List<SongEnglish>?,context: Context): AdapterAlbumDetails {
            this.context = context
            this.listSongAlbumEnglishList = albumEnglishList;
            return AdapterAlbumDetails()
        }

        fun newInstanceAlbumSpanish(albumSpanishList: List<SongSpanish>?,context: Context): AdapterAlbumDetails {
            this.context = context
            this.listSongAlbumSpanishList = albumSpanishList;
            return AdapterAlbumDetails()
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderAlbumDetails {
        val rootView = LayoutInflater.from(p0.context).inflate(R.layout.item_list_song, p0, false)
        return ViewHolderAlbumDetails(rootView)
    }

    override fun getItemCount(): Int {
        if(LocaleHelper().getLanguage(context).equals("en")){
            listSongAlbumEnglishList?.let {
                return it.size
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            listSongAlbumSpanishList?.let {
                return it.size
            }
        }

        return 0
    }

    override fun onBindViewHolder(myViewHolder: ViewHolderAlbumDetails, position: Int) {
        viewHolderAlbum(myViewHolder, position, context)
    }

    private fun viewHolderAlbum(myViewHolder: ViewHolderAlbumDetails, position: Int,context: Context){
        if(LocaleHelper().getLanguage(context).equals("en")){
            listSongAlbumEnglishList.let {
                val item = it?.get(position)
                item?.let {
                    myViewHolder.setValueSongAlbumEnglish(it)
                }
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            listSongAlbumSpanishList.let {
                val item = it?.get(position)
                item?.let {
                    myViewHolder.setValueSongAlbumSpanish(it)
                }
            }
        }
    }

    inner class ViewHolderAlbumDetails(val view: View): RecyclerView.ViewHolder(view) {
        var name: TextView = itemView.textViewListSong

        /*
      Ponemos la información del item en la celda
       */
        fun setValueSongAlbumEnglish(item: SongEnglish) {
            name.setText(item.name)
        }

        fun setValueSongAlbumSpanish(item: SongSpanish) {
            name.setText(item.name)
        }
    }
}