package com.utad.david.androidappmusic

import android.arch.lifecycle.Observer
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.util.Log
import android.view.Menu
import android.widget.Toast
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish
import com.utad.david.androidappmusic.DataRoom.Repositorys.English.RepositoryAlbumsEnglish
import com.utad.david.androidappmusic.DataRoom.Repositorys.English.RepositoryCategoriesEnglish
import com.utad.david.androidappmusic.DataRoom.Repositorys.English.RepositorySongsEnglish
import com.utad.david.androidappmusic.DataRoom.Repositorys.Spanish.RepositoryAlbumsSpanish
import com.utad.david.androidappmusic.DataRoom.Repositorys.Spanish.RepositoryCategoriesSpanish
import com.utad.david.androidappmusic.DataRoom.Repositorys.Spanish.RepositorySongsSpanish
import com.utad.david.androidappmusic.Models.English.AlbumEnglish
import com.utad.david.androidappmusic.Models.English.CategoriesEnglish
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.AlbumSpanish
import com.utad.david.androidappmusic.Models.Spanish.CategoriesSpanish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish
import org.jetbrains.anko.doAsync
import com.utad.david.androidappmusic.DialogFragments.AlbumDetails
import com.utad.david.androidappmusic.DialogFragments.CartFragment
import com.utad.david.androidappmusic.DialogFragments.SongDetails
import com.utad.david.androidappmusic.Fragments.*


class HomeActivity : AppCompatActivity(), AlbumDetails.OnFragmentInteractionListener , SongDetails.OnFragmentInteractionListener{

    private lateinit var toolbar: Toolbar;
    private lateinit var tabLayout:TabLayout;
    private lateinit var language:String;

    private var listCategoriesEnglish: List<CategoriesEnglish>? = listOf();
    private var listCategoriesSpanish: List<CategoriesSpanish>? = listOf();
    private var listSongSpanish: List<SongSpanish>? = listOf();
    private var listSongEnglish: List<SongEnglish>? = listOf();
    private var listAlbumSpanish: List<AlbumSpanish>? = listOf();
    private var listAlbumEnglish: List<AlbumEnglish>? = listOf();


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)

        checkLanguage()
        configureView()
        configureTabbar()
        configureToolbar()
        showHomeFragment()
    }

    private  fun checkLanguage(){

        /*
        Iniciamos la base de datos en segundo plano y los metodos de configuracion dependiendo
        del idioma
         */


        doAsync {
            BDMusicAppEnglish.getInstance(this@HomeActivity.application)

            if(LocaleHelper().getLanguage(this@HomeActivity).equals("en")){
                language = "english"
                loadDataCategoriesEnglish()
                loadDataSongEnglish()
                loadDataAlbumEnglish()
            }else if (LocaleHelper().getLanguage(this@HomeActivity).equals("es")){
                language = "spanish"
                loadDataCategoriesSpanish()
                loadDataSongSpanish()
                loasDataAlbumSpanish()
            }
        }


    }

    /*
    Llamadas a los repositorios de ambos idiomas de la base de datos local
     */

    private fun loadDataCategoriesEnglish(){
        RepositoryCategoriesEnglish.getInstance(application).getAllCategories()?.observe(this@HomeActivity, Observer {
            it?.let {
                listCategoriesEnglish = it;
            }
        })
    }

    private fun loadDataSongEnglish(){
        RepositorySongsEnglish.getInstance(application).getAllSongs()?.observe(this@HomeActivity, Observer {
                it?.let {
                    listSongEnglish = it
                }
        })
    }

    private fun loadDataAlbumEnglish(){
        RepositoryAlbumsEnglish.getInstance(application).getAllAlbums()?.observe(this@HomeActivity, Observer {
                it?.let {
                    listAlbumEnglish = it
                }
        })
    }

    private fun loadDataCategoriesSpanish(){
        RepositoryCategoriesSpanish.getInstance(application).getAllCategories()?.observe(this@HomeActivity, Observer {
            it?.let {
                listCategoriesSpanish = it
            }
        })
    }

    private fun loadDataSongSpanish(){
        RepositorySongsSpanish.getInstance(application).getAllSongs()?.observe(this@HomeActivity, Observer {
            it?.let {
                listSongSpanish = it
            }
        })
    }

    private fun loasDataAlbumSpanish(){
        RepositoryAlbumsSpanish.getInstance(application).getAllAlbums()?.observe(this@HomeActivity, Observer {
            it?.let {
                listAlbumSpanish = it
            }
        })
    }

    private fun configureView(){
        toolbar = findViewById(R.id.toolbar)
        tabLayout = findViewById(R.id.tabsHome)
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
    }


    /*
    Configura la navegacion de las pestañas del tabLayout
     */

    private fun configureTabbar() {
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                p0?.let {
                    when(it.position){
                        0->{
                            showHomeFragment()
                        }
                        1->{
                            showSongFragment(0)
                        }
                        2->{
                            showAlbumFragment(1)
                        }
                        3->{
                            showPlaylistsFragment()
                        }
                        4->{
                            showReleasesFragment()
                        }
                    }
                }
            }

        })
    }

    /*
    Metodos que remplazan el framlayout del activity pot el correspondiente
     */

    private fun showHomeFragment(){
        supportFragmentManager.beginTransaction().also {fragmentTransaction ->
            HomeFragment().apply {
                fragmentTransaction.replace(R.id.frameLayout,this)
                fragmentTransaction.addToBackStack(null)
            }
        }.commit()
    }

    private fun showSongFragment(option:Int){
        supportFragmentManager.beginTransaction().also {fragmentTransaction ->
            AlbumSongFragments.newInstanceSong(option).apply {
                fragmentTransaction.replace(R.id.frameLayout,this)
                fragmentTransaction.addToBackStack(null)
                setListCategorieSpanish(listCategoriesSpanish)
                setListCategorieEnglish(listCategoriesEnglish)
                setListSongSpanish(listSongSpanish)
                setListSongEnglish(listSongEnglish)
            }
        }.commit()
    }

    private fun showAlbumFragment(option:Int){
        supportFragmentManager.beginTransaction().also {fragmentTransaction ->
            AlbumSongFragments.newInstanceAlbum(option).apply {
                fragmentTransaction.replace(R.id.frameLayout,this)
                fragmentTransaction.addToBackStack(null)
                setListCategorieSpanish(listCategoriesSpanish)
                setListCategorieEnglish(listCategoriesEnglish)
                setListAlbumSpanish(listAlbumSpanish)
                setListAlbumEnglish(listAlbumEnglish)
            }
        }.commit()
    }

    private fun showPlaylistsFragment(){
        supportFragmentManager.beginTransaction().also {fragmentTransaction->
            PlaylistFragment().apply {
                fragmentTransaction.replace(R.id.frameLayout,this)
                fragmentTransaction.addToBackStack(null)
            }
        }.commit()
    }

    private fun showReleasesFragment(){
        supportFragmentManager.beginTransaction().also {fragmentTransaction->
            ReleasesFragment().apply {
                fragmentTransaction.replace(R.id.frameLayout,this)
                fragmentTransaction.addToBackStack(null)
            }
        }.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when(id){
            R.id.action_cart -> {
                clickCart()
            }

            R.id.languageEnglish ->{
                clickLenguageEnglish()
            }

            R.id.languageSpanish ->{
                clickLenguageSpanish()
            }
        }
        return true
    }

    /*
        Creamos listas auxiliares de las entidades vacias
     */

    private var listAuxAlbumEnglish: List<AlbumEnglish> = listOf()
    private var listAuxSongEnglish: List<SongEnglish> = listOf()
    private var listAuxAlbumSpanish: List<AlbumSpanish> = listOf()
    private var listAuxSongSpanish: List<SongSpanish> = listOf()

    /*
    callbacks del boton del carrito de los dialogos de album y canciones. Se llama a las funciones
    setters donde pasas la lista auxiliar anteriormente declarada añadiendo el objecto pulsado.
     */

    override fun clickCartButtonEnglishAlbum(albumEnglish: AlbumEnglish) {
        listAuxAlbumEnglish += albumEnglish
        CartFragment.apply {
            setListAlbumEnglish(listAuxAlbumEnglish)
        }
    }

    override fun clickCartButtonSpanishAlbum(albumSpanish: AlbumSpanish) {
        listAuxAlbumSpanish += albumSpanish
        CartFragment.apply {
            setListAlbumSpanish(listAuxAlbumSpanish)
        }
    }

    override fun clickCartButtonEnglishSong(songEnglish: SongEnglish) {
        listAuxSongEnglish += songEnglish
        CartFragment.apply {
            setListSongEnglish(listAuxSongEnglish)
        }
    }

    override fun clickCartButtonSpanishSong(songSpanish: SongSpanish) {
        listAuxSongSpanish += songSpanish
        CartFragment.apply {
            setListSongSpanish(listAuxSongSpanish)
        }
    }

    /*
    Diferencia el idioma y muestra el fragment del carrito correspondiente
     */

    private fun clickCart(): Boolean {
        if(language.equals("english")){
            CartFragment.newInstanceCartEnglish().show(supportFragmentManager,"CARD")
        }else if(language.equals("spanish")) {
            CartFragment.newInstanceCartSpanish().show(supportFragmentManager,"CARD")
        }
        return true
    }

    /*
    Metodos de cambio de idioma del submenu. En estos se comprueba el idioma y en caso de que sea distinto, este se cambia, reiniciando la aplicacion.
    En caso incorrecto se le mostrara al usuario que ya se indica en ese idioma.
     */

    private fun clickLenguageEnglish(){
        if(language.equals("english")){
            Toast.makeText(this, getString(R.string.info_lenguage), Toast.LENGTH_LONG).show()
        }else if(language.equals("spanish")){
            LocaleHelper().setLocale(this,"en")
            val intent = intent
            finish()
            startActivity(intent)
        }
    }

    private fun clickLenguageSpanish(){
        if(language.equals("spanish")){
            Toast.makeText(this, getString(R.string.info_lenguage), Toast.LENGTH_LONG).show()
        }else if(language.equals("english")){
            LocaleHelper().setLocale(this,"es")
            val intent = intent
            finish()
            startActivity(intent)
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleHelper().onAttach(newBase))
    }

    /*
        Declaracion de mutablelists para la funcionalidad de playlist
     */

    private var mutableListAlbumEnglish: MutableList<AlbumEnglish> = mutableListOf()
    private var mutableListAlbumSpanish: MutableList<AlbumSpanish> = mutableListOf()
    private var mutableListSongEnglish: MutableList<SongEnglish> = mutableListOf()
    private var mutableListSongSpanish: MutableList<SongSpanish> = mutableListOf()

    /*
        callbacks del boton del favorito de los dialogos de album y canciones. Se llama a las funciones
         setters donde pasas la mutablelist anteriormente declarada añadiendo el objecto pulsado.
     */

    override fun clickFavoriteSpanishAlbum(albumSpanish: AlbumSpanish) {
        mutableListAlbumSpanish.add(albumSpanish)
        Log.d("FAVORITOS",""+mutableListAlbumSpanish.size)
        PlaylistFragment.apply {
            setMutableListAlbumSpanish(mutableListAlbumSpanish)
        }
    }

    override fun clickFavoriteEnglisAlbum(albumEnglish: AlbumEnglish) {
        mutableListAlbumEnglish.add(albumEnglish)
        Log.d("FAVORITOS",""+mutableListAlbumEnglish.size)
        PlaylistFragment.apply {
            setMutableListAlbumEnglish(mutableListAlbumEnglish)
        }
    }

    override fun clickFavoriteSpanishSong(songSpanish: SongSpanish) {
        mutableListSongSpanish.add(songSpanish)
        Log.d("FAVORITOS",""+mutableListSongSpanish.size)
        PlaylistFragment.apply {
            setMutableListSongSpanish(mutableListSongSpanish)
        }
    }

    override fun clickFavoriteEnglisSong(songEnglish: SongEnglish) {
        mutableListSongEnglish.add(songEnglish)
        Log.d("FAVORITOS",""+mutableListSongEnglish.size)
        PlaylistFragment.apply {
            setMutableListSongEnglish(mutableListSongEnglish)
        }
    }

}
