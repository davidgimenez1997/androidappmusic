package com.utad.david.androidappmusic.DataRoom

import android.app.Application
import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.utad.david.androidappmusic.BuildConfig
import com.utad.david.androidappmusic.DataRoom.ConvertData.ConvertArrayListToObject
import com.utad.david.androidappmusic.DataRoom.Daos.English.*
import com.utad.david.androidappmusic.DataRoom.Daos.Spanish.*
import com.utad.david.androidappmusic.Models.English.*
import com.utad.david.androidappmusic.Models.Spanish.*
import com.utad.david.androidappmusic.R
import org.jetbrains.anko.doAsync

@Database(
    entities = [AlbumEnglish::class, PlaylistEnglish::class, ReleaseEnglish::class, SongEnglish::class, CategoriesEnglish::class,
    AlbumSpanish::class,PlaylistSpanish::class,ReleaseSpanish::class,SongSpanish::class,CategoriesSpanish::class],
    version = 1
)
@TypeConverters(ConvertArrayListToObject::class)
abstract class BDMusicAppEnglish : RoomDatabase() {

    abstract fun albumDaoEnglish(): DaoAlbumsEnglish
    abstract fun playlistDaoEnglish(): DaoPlaylistEnglish
    abstract fun releaseDaoEnglish(): DaoReleaseEnglish
    abstract fun songDaoEnglish(): DaoSongsEnglish
    abstract fun categoryDaoEnglish(): DaoCategoriesEnglish

    abstract fun albumDaoSpanish(): DaoAlbumsSpanish
    abstract fun playlistDaoSpanish(): DaoPlaylistSpanish
    abstract fun releaseDaoSpanish(): DaoReleaseSpanish
    abstract fun songDaoSpanish(): DaoSongsSpanish
    abstract fun categoryDaoSpanish(): DaoCategoriesSpanish

    companion object {
        // Get DB name from constant configuration
        private val DB_NAME = BuildConfig.DATABASE_NAME
        @Volatile
        private var INSTANCE: BDMusicAppEnglish? = null
        private var listCategoriesEnglish:List<CategoriesEnglish> = listOf()
        private var listCategoriesSpanish:List<CategoriesSpanish> = listOf()



        fun getInstance(context: Application): BDMusicAppEnglish =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        fun destroyInstance() {
            INSTANCE = null
        }

        /*
        Metodo que crea la base de datos
         */

        // Build database and initialize
        private fun buildDatabase(context: Application) =
        // ONLY FOR TEST. Change to databaseBuilder()
            Room.databaseBuilder(context, BDMusicAppEnglish::class.java, DB_NAME)
                // Wipes and rebuilds instead of migrating
                // if no Migration object.
                .fallbackToDestructiveMigration()
                .addCallback(sRoomDatabaseCallbackCategoryEnglish)
                .addCallback(sRoomDatabaseCallbackCategorySpanish)
                .addCallback(sRoomDatabaseCallbackSongEnglish)
                .addCallback(sRoomDatabaseCallbackSongSpanish)
                .addCallback(sRoomDatabaseCallbackAlbumEnglish)
                .addCallback(sRoomDatabaseCallbackAlbumSpanish)
                .addCallback(sRoomDatabaseCallbackReleaseEnglish)
                .addCallback(sRoomDatabaseCallbackReleaseSpanish)
                .addCallback(sRoomDatabaseCallbackPlaylist)
                .build()


        /*
        Callbacks de la base de datos con la diferente informacion
         */

        private val sRoomDatabaseCallbackCategoryEnglish = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)

                INSTANCE?.let { database ->
                    doAsync {
                        database.categoryDaoEnglish().run {
                            listCategoriesEnglish = listOf(
                                CategoriesEnglish("All"),
                                CategoriesEnglish("Rock"),
                                CategoriesEnglish("Rap"),
                                CategoriesEnglish("Pop"),
                                CategoriesEnglish("Techno"),
                                CategoriesEnglish("Reggae"),
                                CategoriesEnglish("Metal")
                            )
                            this.deleteAll()
                            this.insertAll(listCategoriesEnglish)
                        }
                    }
                }
            }
        }

        private val sRoomDatabaseCallbackCategorySpanish = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)

                INSTANCE?.let { database ->
                    doAsync {
                        database.categoryDaoSpanish().run {
                            listCategoriesSpanish = listOf(
                                CategoriesSpanish("Todos"),
                                CategoriesSpanish("Rock"),
                                CategoriesSpanish("Rap"),
                                CategoriesSpanish("Pop"),
                                CategoriesSpanish("Techno"),
                                CategoriesSpanish("Reggae"),
                                CategoriesSpanish("Metal")
                            )
                            this.deleteAll()
                            this.insertAll(listCategoriesSpanish)
                        }
                    }
                }
            }
        }

        private val sRoomDatabaseCallbackAlbumEnglish = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)

                INSTANCE?.let { database ->
                    doAsync {
                        database.albumDaoEnglish().run {

                            var listSongs1 = listOf(
                                SongEnglish(
                                    "Come Out Swinging"
                                    , "https://images.rapgenius.com/af6add82fbbb3f33f8995f1f64511845.350x489x1.jpg"
                                    , 2.47
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99
                                     ,false
                                )
                                , SongEnglish(
                                    "Original Prankster"
                                    , "https://images-na.ssl-images-amazon.com/images/I/51zmtsynVNL._SX355_.jpg"
                                    , 3.42
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99
                                    ,false

                                )
                                ,
                                SongEnglish(
                                    "Want you bad"
                                    , "https://upload.wikimedia.org/wikipedia/en/3/3d/Want_you_bad.jpg"
                                    , 3.23
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "Million miles away"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/4/4f/Million_Miles_Single.jpg/220px-Million_Miles_Single.jpg"
                                    , 3.40
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "Dammit, i changed again"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeanrNHrJwf1X5UrAUFz5Yk7aVP5ghBjA63NvxtGp5A8N9UmEDRg"
                                    , 2.49
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "Living in Chaos"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 3.28
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "Special Delivery"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 3.00
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99
                                    ,false

                                )
                            )

                            var listSongs2 = listOf(
                                SongEnglish(
                                    "One Fine Day"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBSjbKoRw_MdumPcSnXqxqQwXxs-kcL-LQoNwC2GUWyVIFOeXDug"
                                    , 2.45
                                    , "The Offspring [Conspirancy of one]"
                                    , "TheOffspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "All along"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTveVlcKopVQlTwIKcVxnBEB_iRXuF-UHm4QD7ZVEbyxSLONd8V"
                                    , 1.39
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "Denial, revisited"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 4.33
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "Vultures"
                                    , "https://www.teleadhesivo.com/es/img/asmu178-jpg/folder/products-listado-merchanthover/pegatinas-coches-motos-the-offspring---vultures.jpg"
                                    , 3.35
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "Juicy"
                                    , "https://www.pictorem.com/collection/900_Dan--Avenell_A1Biggie%20signed.jpg"
                                    , 4.11
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "Big Poppa"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSATZXfASpWUUNwJh6mNyWEX_tYfLDn64P4oo4PPlWz-EV8d_kjeQ"
                                    , 4.22
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "Ten Crack Commandments"
                                    , "https://i4.cdn.hhv.de/catalog/475x475/00140/140849.jpg"
                                    , 3.26
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "Suicidal Thoughts"
                                    , "https://www.pictorem.com/collection/900_Dan--Avenell_A1Biggie%20signed.jpg"
                                    , 2.54
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99
                                    ,false

                                ),
                                SongEnglish(
                                    "Party & Bullshit"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/d/d3/BigPandBSingle.jpg/220px-BigPandBSingle.jpg"
                                    , 3.42
                                    , "Notorius big - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99
                                    ,false

                                )
                            )

                            var list = listOf(
                                AlbumEnglish(
                                    "Conspirancy Of One",
                                    "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg",
                                    "Most famous Offspring album",
                                    "The Offspring",
                                    listCategoriesEnglish.get(1).name,
                                    listSongs1
                                    ,4.99
                                    ,false

                                ),
                                AlbumEnglish(
                                    "Biggie smalls best hits",
                                    "https://www.pictorem.com/collection/900_Dan--Avenell_A1Biggie%20signed.jpg",
                                    "Biggie Smalls is back with this recopilation of his best songs",
                                    "Notorius B.I.G",
                                    listCategoriesEnglish.get(2).name,
                                    listSongs2
                                    ,4.99
                                    ,false


                                ),
                                AlbumEnglish(
                                    "Starboy",
                                    "https://images-na.ssl-images-amazon.com/images/I/51UVW34rJTL._SY355_.jpg",
                                    "The weekend's new album,2018",
                                    "The Weekend",
                                    listCategoriesEnglish.get(3).name,
                                    listSongs1
                                    ,4.99
                                    ,false


                                ),
                                AlbumEnglish(
                                    "Fisher - Top Tracks",
                                    "http://techno-import.fr/shop/img/p/2/5/7/9/7/25797-thickbox_default.jpg",
                                    "Fisher's most famous songs",
                                    "Fisher",
                                    listCategoriesEnglish.get(4).name,
                                    listSongs2
                                    ,4.99
                                    ,false


                                ),
                                AlbumEnglish(
                                    "Damian Marley - Welcome to Jamrock",
                                    "https://images-na.ssl-images-amazon.com/images/I/61BtM6DXIFL.jpg",
                                    "Feel the reggae music",
                                    "Damian Marley",
                                    listCategoriesEnglish.get(5).name,
                                    listSongs1
                                    ,4.99
                                    ,false


                                ),
                                AlbumEnglish(
                                    "Master of Puppets",
                                    "https://img.cdandlp.com/2017/10/imgL/118972381.jpg",
                                    "Go back to the past and rememorate one of the best albums ever made by metallica",
                                    "Metallica",
                                    listCategoriesEnglish.get(6).name,
                                    listSongs2
                                    ,4.99
                                    ,false


                                )
                            )
                            this.deleteAll()
                            this.insertAll(list)
                        }
                    }
                }

            }
        }

        private val sRoomDatabaseCallbackAlbumSpanish = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)

                INSTANCE?.let { database ->
                    doAsync {
                        database.albumDaoSpanish().run {

                            var listSongs1 = listOf(
                                SongSpanish(
                                    "Ya kidding"
                                    , "https://img.discogs.com/XhP3Ukhhdwz_ZoY-1RSFgYkvb2Q=/fit-in/600x606/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-10774669-1504169945-4385.jpeg.jpg"
                                    , 4.32
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesSpanish.get(4).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Losing it"
                                    , "https://i.ytimg.com/vi/_tfi3dDSQG4/hqdefault.jpg"
                                    , 4.09
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesSpanish.get(4).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Burnin up"
                                    , "https://i.ytimg.com/vi/EOdVgt2bgvA/maxresdefault.jpg"
                                    , 5.03
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesSpanish.get(4).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Ya didn't"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcESdvGPy2GG9_afBgVkijpsidzjlJpmCoQDCnci97l61-YnsU"
                                    , 5.50
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesSpanish.get(4).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Confrontation"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 5.29
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "There for you"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 4.41
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Welcome to Jamrock"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 3.33
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99

                                    ,false

                                ),
                                SongSpanish(
                                    "The Master has come back"
                                    , "https://img.discogs.com/nc4OcqY_nyIJIPtiPUeKMiuCDRc=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-2585585-1299156932.jpeg.jpg"
                                    , 4.41
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "All night"
                                    , "https://m.media-amazon.com/images/I/61RicEE+uZL._SS500_.jpg"
                                    , 3.30
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Beautiful"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 4.48
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Battery"
                                    , "https://i.ytimg.com/vi/VDwl2jfv-RU/maxresdefault.jpg"
                                    , 5.10
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesSpanish.get(6).name
                                    ,1.99
                                    ,false

                                )
                            )

                            var listSongs2 = listOf(

                                SongSpanish(
                                    "Come Out Swinging"
                                    , "https://images.rapgenius.com/af6add82fbbb3f33f8995f1f64511845.350x489x1.jpg"
                                    , 2.47
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99
                                    ,false

                                )
                                , SongSpanish(
                                    "Original Prankster"
                                    , "https://images-na.ssl-images-amazon.com/images/I/51zmtsynVNL._SX355_.jpg"
                                    , 3.42
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99
                                    ,false

                                )
                                ,
                                SongSpanish(
                                    "Want you bad"
                                    , "https://upload.wikimedia.org/wikipedia/en/3/3d/Want_you_bad.jpg"
                                    , 3.23
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Million miles away"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/4/4f/Million_Miles_Single.jpg/220px-Million_Miles_Single.jpg"
                                    , 3.40
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Dammit, i changed again"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeanrNHrJwf1X5UrAUFz5Yk7aVP5ghBjA63NvxtGp5A8N9UmEDRg"
                                    , 2.49
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Living in Chaos"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 3.28
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Special Delivery"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 3.00
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "One Fine Day"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBSjbKoRw_MdumPcSnXqxqQwXxs-kcL-LQoNwC2GUWyVIFOeXDug"
                                    , 2.45
                                    , "The Offspring [Conspirancy of one]"
                                    , "TheOffspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "All along"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTveVlcKopVQlTwIKcVxnBEB_iRXuF-UHm4QD7ZVEbyxSLONd8V"
                                    , 1.39
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Denial, revisited"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 4.33
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Vultures"
                                    , "https://www.teleadhesivo.com/es/img/asmu178-jpg/folder/products-listado-merchanthover/pegatinas-coches-motos-the-offspring---vultures.jpg"
                                    , 3.35
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Juicy"
                                    , "https://www.pictorem.com/collection/900_Dan--Avenell_A1Biggie%20signed.jpg"
                                    , 4.11
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99
                                    ,false


                                ),
                                SongSpanish(
                                    "Big Poppa"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSATZXfASpWUUNwJh6mNyWEX_tYfLDn64P4oo4PPlWz-EV8d_kjeQ"
                                    , 4.22
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Ten Crack Commandments"
                                    , "https://i4.cdn.hhv.de/catalog/475x475/00140/140849.jpg"
                                    , 3.26
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Suicidal Thoughts"
                                    , "https://www.pictorem.com/collection/900_Dan--Avenell_A1Biggie%20signed.jpg"
                                    , 2.54
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99
                                    ,false

                                ),
                                SongSpanish(
                                    "Party & Bullshit"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/d/d3/BigPandBSingle.jpg/220px-BigPandBSingle.jpg"
                                    , 3.42
                                    , "Notorius big - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99                                ,false

                                )
                            )

                            var list = listOf(
                                AlbumSpanish(
                                    "Conspirancy Of One",
                                    "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg",
                                    "Most famous Offspring album",
                                    "The Offspring",
                                    listCategoriesSpanish.get(1).name,
                                    listSongs1
                                    ,4.99                                ,false


                                ),
                                AlbumSpanish(
                                    "Biggie smalls best hits",
                                    "https://www.pictorem.com/collection/900_Dan--Avenell_A1Biggie%20signed.jpg",
                                    "Biggie Smalls is back with this recopilation of his best songs",
                                    "Notorius B.I.G",
                                    listCategoriesSpanish.get(2).name,
                                    listSongs2
                                    ,4.99                                ,false


                                ),
                                AlbumSpanish(
                                    "Starboy",
                                    "https://images-na.ssl-images-amazon.com/images/I/51UVW34rJTL._SY355_.jpg",
                                    "The weekend's new album,2018",
                                    "The Weekend",
                                    listCategoriesSpanish.get(3).name,
                                    listSongs1
                                    ,4.99                                ,false


                                ),
                                AlbumSpanish(
                                    "Fisher - Top Tracks",
                                    "http://techno-import.fr/shop/img/p/2/5/7/9/7/25797-thickbox_default.jpg",
                                    "Fisher's most famous songs",
                                    "Fisher",
                                    listCategoriesSpanish.get(4).name,
                                    listSongs2
                                    ,4.99                                ,false


                                ),
                                AlbumSpanish(
                                    "Damian Marley - Welcome to Jamrock",
                                    "https://images-na.ssl-images-amazon.com/images/I/61BtM6DXIFL.jpg",
                                    "Feel the reggae music",
                                    "Damian Marley",
                                    listCategoriesSpanish.get(5).name,
                                    listSongs1
                                    ,4.99                                ,false


                                ),
                                AlbumSpanish(
                                    "Master of Puppets",
                                    "https://img.cdandlp.com/2017/10/imgL/118972381.jpg",
                                    "Go back to the past and rememorate one of the best albums ever made by metallica",
                                    "Metallica",
                                    listCategoriesSpanish.get(6).name,
                                    listSongs2
                                    ,4.99                                ,false

                                )
                            )
                            this.deleteAll()
                            this.insertAll(list)
                        }
                    }
                }

            }
        }

        private val sRoomDatabaseCallbackPlaylist = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)

                INSTANCE?.let { database ->
                    doAsync {
                        database.playlistDaoEnglish().run {
                            var listSongs = listOf(
                                SongEnglish(
                                    "Vultures"
                                    , "https://www.teleadhesivo.com/es/img/asmu178-jpg/folder/products-listado-merchanthover/pegatinas-coches-motos-the-offspring---vultures.jpg"
                                    , 3.35
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99                                ,false

                                )
                            )
                            var list = listOf(
                                PlaylistEnglish(
                                    "nombre",
                                    0, "d", "d", listSongs
                                )
                            )
                            this.deleteAll()
                            this.insertAll(list)
                        }
                    }
                }
            }
        }


        private val sRoomDatabaseCallbackReleaseEnglish = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)

                INSTANCE?.let { database ->
                    doAsync {
                        database.releaseDaoEnglish().run {
                            var list = listOf(
                                ReleaseEnglish(
                                   "https://cdn.viagogo.net/img/cat/4375/1/37.jpg",
                                    "The SSE Arena Wembley, Londres, Reino Unido. 01 june 2019 18:00 "
                                ),
                                ReleaseEnglish(
                                    "https://cdn.viagogo.net/img/cat/31910/1/37.jpg",
                                    "The SSE Arena Wembley, Londres, Reino Unido. 22 may 2019 18:00"
                                ),
                                ReleaseEnglish(
                                    "https://cdn.viagogo.net/img/cat/4798/1/37.jpg",
                                    "Wembley Stadium, Londres, Reino Unido. 21 june 2019 19:00"
                                ),
                                ReleaseEnglish(
                                   "https://cdn.viagogo.net/img/cat/8284/3/37.jpg",
                                    "The SSE Arena Wembley, Londres, Reino Unido.  16 february 2019 18:00"
                                )
                            )
                            this.deleteAll()
                            this.insertAll(list)
                        }
                    }
                }
            }
        }

        private val sRoomDatabaseCallbackReleaseSpanish = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)

                INSTANCE?.let { database ->
                    doAsync {
                        database.releaseDaoSpanish().run {
                            var list = listOf(
                                ReleaseSpanish(
                                    "https://www.elindependiente.com/wp-content/uploads/2018/12/Mezzanine-XX1-1440x1080.jpg",
                                    "16 de febrero en el Sant Jordi Club de Barcelona y el 17 de febrero en el Palacio Vistalegre de Madrid."
                                ),
                                ReleaseSpanish(
                                   "https://www.elindependiente.com/wp-content/uploads/2018/12/Slash-en-Madrid-2015-1440x1080.jpg",
                                    "12 de marzo en el Sant Jordi Club de Barcelona y el 13 de marzo en el WiZink Center de Madrid"
                                ),
                                ReleaseSpanish(
                                    "https://www.elindependiente.com/wp-content/uploads/2018/12/Twenty-One-Pilots-en-Tampa-1440x1080.jpg"
                                ,"15 de marzo en Bilbao (BEC) y el 16 de marzo en Madrid (WiZink Center, entradas ya agotadas)"
                                ),
                                ReleaseSpanish(
                                    "https://www.elindependiente.com/wp-content/uploads/2018/12/Eros-Ramazotti-1440x1080.jpg",
                                    "21 de marzo en el Palacio Vistalegre de Madrid y el 23 de marzo en el Palau Sant Jordi de Barcelona"
                                )
                            )
                            this.deleteAll()
                            this.insertAll(list)
                        }
                    }
                }
            }
        }

        private val sRoomDatabaseCallbackSongSpanish = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)

                INSTANCE?.let { database ->
                    doAsync {
                        database.songDaoSpanish().run {
                            var list = listOf(
                                SongSpanish(
                                    "Come Out Swinging"
                                    , "https://images.rapgenius.com/af6add82fbbb3f33f8995f1f64511845.350x489x1.jpg"
                                    , 2.47
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99                                ,false

                                )
                                , SongSpanish(
                                    "Original Prankster"
                                    , "https://images-na.ssl-images-amazon.com/images/I/51zmtsynVNL._SX355_.jpg"
                                    , 3.42
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99                                ,false

                                )
                                ,
                                SongSpanish(
                                    "Want you bad"
                                    , "https://upload.wikimedia.org/wikipedia/en/3/3d/Want_you_bad.jpg"
                                    , 3.23
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Million miles away"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/4/4f/Million_Miles_Single.jpg/220px-Million_Miles_Single.jpg"
                                    , 3.40
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Dammit, i changed again"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeanrNHrJwf1X5UrAUFz5Yk7aVP5ghBjA63NvxtGp5A8N9UmEDRg"
                                    , 2.49
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Living in Chaos"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 3.28
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Special Delivery"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 3.00
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "One Fine Day"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBSjbKoRw_MdumPcSnXqxqQwXxs-kcL-LQoNwC2GUWyVIFOeXDug"
                                    , 2.45
                                    , "The Offspring [Conspirancy of one]"
                                    , "TheOffspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "All along"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTveVlcKopVQlTwIKcVxnBEB_iRXuF-UHm4QD7ZVEbyxSLONd8V"
                                    , 1.39
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Denial, revisited"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 4.33
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Vultures"
                                    , "https://www.teleadhesivo.com/es/img/asmu178-jpg/folder/products-listado-merchanthover/pegatinas-coches-motos-the-offspring---vultures.jpg"
                                    , 3.35
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesSpanish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Juicy"
                                    , "https://www.pictorem.com/collection/900_Dan--Avenell_A1Biggie%20signed.jpg"
                                    , 4.11
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Big Poppa"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSATZXfASpWUUNwJh6mNyWEX_tYfLDn64P4oo4PPlWz-EV8d_kjeQ"
                                    , 4.22
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Ten Crack Commandments"
                                    , "https://i4.cdn.hhv.de/catalog/475x475/00140/140849.jpg"
                                    , 3.26
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Suicidal Thoughts"
                                    , "https://www.pictorem.com/collection/900_Dan--Avenell_A1Biggie%20signed.jpg"
                                    , 2.54
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Party & Bullshit"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/d/d3/BigPandBSingle.jpg/220px-BigPandBSingle.jpg"
                                    , 3.42
                                    , "Notorius big - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Hypnotyze"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/1/1e/BiggieHypnotize.jpg/220px-BiggieHypnotize.jpg"
                                    , 3.52
                                    , "Notorius big - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Who shot ya?"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSihuoyDRR19HFtFN3WFnSaaje2RCcVxWyDd4b_sWI4Clg3ZJfL"
                                    , 5.21
                                    , "Notorius big - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Mo Money Mo Problems"
                                    , "https://upload.wikimedia.org/wikipedia/en/9/9d/Biggiemomoneymoproblems.jpg"
                                    , 5.48
                                    , "Notorius big - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesSpanish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Starboy"
                                    , "https://images-na.ssl-images-amazon.com/images/I/51UVW34rJTL._SY355_.jpg"
                                    , 3.50
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesSpanish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Party Monster"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/d/d7/The_Weeknd_-_Party_Monster.jpg/220px-The_Weeknd_-_Party_Monster.jpg"
                                    , 4.09
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesSpanish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "False Alarm"
                                    , "https://static.highsnobiety.com/wp-content/uploads/2016/09/29155435/the-weeknd-false-alarm-01-480x320.jpg"
                                    , 3.50
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesSpanish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Reminder"
                                    , "https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2F18632b71a26c116bdc7917574b38da9a.1000x1000x1.jpg"
                                    , 3.38
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesSpanish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Rockin'"
                                    , "https://www.lahiguera.net/musicalia/artistas/the_weeknd/disco/7948/tema/14669/the_weeknd_rockin-portada.jpg"
                                    , 3.52
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesSpanish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Secrets"
                                    , "https://i1.wp.com/68.media.tumblr.com/8633025b3bdc1770e1af5ac727aa5ff4/tumblr_orqd1vjHE31qln5jgo1_500.jpg?w=744&ssl=1"
                                    , 4.25
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesSpanish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "True Colors"
                                    , "https://i.ytimg.com/vi/0rF-KTT9JeE/maxresdefault.jpg"
                                    , 3.26
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesSpanish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Stop it"
                                    , "https://geo-media.beatport.com/image/405a8943-c55a-48ef-b1ad-9df015b5e5e8.jpg"
                                    , 6.25
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesSpanish.get(4).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Ya kidding"
                                    , "https://img.discogs.com/XhP3Ukhhdwz_ZoY-1RSFgYkvb2Q=/fit-in/600x606/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-10774669-1504169945-4385.jpeg.jpg"
                                    , 4.32
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesSpanish.get(4).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Losing it"
                                    , "https://i.ytimg.com/vi/_tfi3dDSQG4/hqdefault.jpg"
                                    , 4.09
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesSpanish.get(4).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Burnin up"
                                    , "https://i.ytimg.com/vi/EOdVgt2bgvA/maxresdefault.jpg"
                                    , 5.03
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesSpanish.get(4).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Ya didn't"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcESdvGPy2GG9_afBgVkijpsidzjlJpmCoQDCnci97l61-YnsU"
                                    , 5.50
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesSpanish.get(4).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Confrontation"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 5.29
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "There for you"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 4.41
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Welcome to Jamrock"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 3.33
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "The Master has come back"
                                    , "https://img.discogs.com/nc4OcqY_nyIJIPtiPUeKMiuCDRc=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-2585585-1299156932.jpeg.jpg"
                                    , 4.41
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "All night"
                                    , "https://m.media-amazon.com/images/I/61RicEE+uZL._SS500_.jpg"
                                    , 3.30
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Beautiful"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 4.48
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesSpanish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Battery"
                                    , "https://i.ytimg.com/vi/VDwl2jfv-RU/maxresdefault.jpg"
                                    , 5.10
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesSpanish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Master of Puppets"
                                    , "https://img.cdandlp.com/2017/10/imgL/118972381.jpg"
                                    , 8.38
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesSpanish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "The thing that should not be"
                                    , "https://www.nacionrock.com/wp-content/uploads/metthething.jpg"
                                    , 6.32
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesSpanish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Welcome Home(Sanitarium)"
                                    , "https://img.discogs.com/hIUghWJ0XiXR2uU1e_M5RuskDyU=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-6105580-1430663138-4611.jpeg.jpg"
                                    , 6.28
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesSpanish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Disposable Heroes"
                                    , "https://img.cdandlp.com/2017/10/imgL/118972381.jpg"
                                    , 8.14
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesSpanish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Leper Messiah"
                                    , "https://i.ytimg.com/vi/K1lBA2Ebxx4/hqdefault.jpg"
                                    , 5.38
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesSpanish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Orion"
                                    , "https://i.ytimg.com/vi/cVhkKREAxss/hqdefault.jpg"
                                    , 8.12
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesSpanish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongSpanish(
                                    "Damage, Inc."
                                    , "https://www.heavymetalmerchant.com/assets/full/RA_TP157.jpg"
                                    , 5.08
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesSpanish.get(6).name
                                    ,1.99                                ,false

                                )

                            )
                            this.deleteAll()
                            this.insertAll(list)
                        }
                    }
                }
            }
        }

        private val sRoomDatabaseCallbackSongEnglish = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)

                INSTANCE?.let { database ->
                    doAsync {
                        database.songDaoEnglish().run {
                            var list = listOf(
                                SongEnglish(
                                    "Come Out Swinging"
                                    , "https://images.rapgenius.com/af6add82fbbb3f33f8995f1f64511845.350x489x1.jpg"
                                    , 2.47
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99                                ,false

                                )
                                , SongEnglish(
                                    "Original Prankster"
                                    , "https://images-na.ssl-images-amazon.com/images/I/51zmtsynVNL._SX355_.jpg"
                                    , 3.42
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99                                ,false

                                )
                                ,
                                SongEnglish(
                                    "Want you bad"
                                    , "https://upload.wikimedia.org/wikipedia/en/3/3d/Want_you_bad.jpg"
                                    , 3.23
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Million miles away"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/4/4f/Million_Miles_Single.jpg/220px-Million_Miles_Single.jpg"
                                    , 3.40
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Dammit, i changed again"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeanrNHrJwf1X5UrAUFz5Yk7aVP5ghBjA63NvxtGp5A8N9UmEDRg"
                                    , 2.49
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99      ,false

                                ),
                                SongEnglish(
                                    "Living in Chaos"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 3.28
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Special Delivery"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 3.00
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "One Fine Day"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBSjbKoRw_MdumPcSnXqxqQwXxs-kcL-LQoNwC2GUWyVIFOeXDug"
                                    , 2.45
                                    , "The Offspring [Conspirancy of one]"
                                    , "TheOffspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "All along"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTveVlcKopVQlTwIKcVxnBEB_iRXuF-UHm4QD7ZVEbyxSLONd8V"
                                    , 1.39
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Denial, revisited"
                                    , "https://rockpeperina.com/wp-content/uploads/2017/11/TheOffspringConspiracyOfOneEdit-678x381.jpg"
                                    , 4.33
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Vultures"
                                    , "https://www.teleadhesivo.com/es/img/asmu178-jpg/folder/products-listado-merchanthover/pegatinas-coches-motos-the-offspring---vultures.jpg"
                                    , 3.35
                                    , "The Offspring [Conspirancy of one]"
                                    , "The Offspring"
                                    , listCategoriesEnglish.get(1).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Juicy"
                                    , "https://www.pictorem.com/collection/900_Dan--Avenell_A1Biggie%20signed.jpg"
                                    , 4.11
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Big Poppa"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSATZXfASpWUUNwJh6mNyWEX_tYfLDn64P4oo4PPlWz-EV8d_kjeQ"
                                    , 4.22
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Ten Crack Commandments"
                                    , "https://i4.cdn.hhv.de/catalog/475x475/00140/140849.jpg"
                                    , 3.26
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Suicidal Thoughts"
                                    , "https://www.pictorem.com/collection/900_Dan--Avenell_A1Biggie%20signed.jpg"
                                    , 2.54
                                    , "Notorius B.I.G - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Party & Bullshit"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/d/d3/BigPandBSingle.jpg/220px-BigPandBSingle.jpg"
                                    , 3.42
                                    , "Notorius big - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Hypnotyze"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/1/1e/BiggieHypnotize.jpg/220px-BiggieHypnotize.jpg"
                                    , 3.52
                                    , "Notorius big - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Who shot ya?"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSihuoyDRR19HFtFN3WFnSaaje2RCcVxWyDd4b_sWI4Clg3ZJfL"
                                    , 5.21
                                    , "Notorius big - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Mo Money Mo Problems"
                                    , "https://upload.wikimedia.org/wikipedia/en/9/9d/Biggiemomoneymoproblems.jpg"
                                    , 5.48
                                    , "Notorius big - [Notorius Big - Best Hits]"
                                    , "Notorius B.I.G"
                                    , listCategoriesEnglish.get(2).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Starboy"
                                    , "https://images-na.ssl-images-amazon.com/images/I/51UVW34rJTL._SY355_.jpg"
                                    , 3.50
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesEnglish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Party Monster"
                                    , "https://upload.wikimedia.org/wikipedia/en/thumb/d/d7/The_Weeknd_-_Party_Monster.jpg/220px-The_Weeknd_-_Party_Monster.jpg"
                                    , 4.09
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesEnglish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "False Alarm"
                                    , "https://static.highsnobiety.com/wp-content/uploads/2016/09/29155435/the-weeknd-false-alarm-01-480x320.jpg"
                                    , 3.50
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesEnglish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Reminder"
                                    , "https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2F18632b71a26c116bdc7917574b38da9a.1000x1000x1.jpg"
                                    , 3.38
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesEnglish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Rockin'"
                                    , "https://www.lahiguera.net/musicalia/artistas/the_weeknd/disco/7948/tema/14669/the_weeknd_rockin-portada.jpg"
                                    , 3.52
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesEnglish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Secrets"
                                    , "https://i1.wp.com/68.media.tumblr.com/8633025b3bdc1770e1af5ac727aa5ff4/tumblr_orqd1vjHE31qln5jgo1_500.jpg?w=744&ssl=1"
                                    , 4.25
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesEnglish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "True Colors"
                                    , "https://i.ytimg.com/vi/0rF-KTT9JeE/maxresdefault.jpg"
                                    , 3.26
                                    , "The Weekend - [Starboy]"
                                    , "The Weekend"
                                    , listCategoriesEnglish.get(3).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Stop it"
                                    , "https://geo-media.beatport.com/image/405a8943-c55a-48ef-b1ad-9df015b5e5e8.jpg"
                                    , 6.25
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesEnglish.get(4).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Ya kidding"
                                    , "https://img.discogs.com/XhP3Ukhhdwz_ZoY-1RSFgYkvb2Q=/fit-in/600x606/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-10774669-1504169945-4385.jpeg.jpg"
                                    , 4.32
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesEnglish.get(4).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Losing it"
                                    , "https://i.ytimg.com/vi/_tfi3dDSQG4/hqdefault.jpg"
                                    , 4.09
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesEnglish.get(4).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Burnin up"
                                    , "https://i.ytimg.com/vi/EOdVgt2bgvA/maxresdefault.jpg"
                                    , 5.03
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesEnglish.get(4).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Ya didn't"
                                    , "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcESdvGPy2GG9_afBgVkijpsidzjlJpmCoQDCnci97l61-YnsU"
                                    , 5.50
                                    , "Fisher - [Top Tracks]"
                                    , "Fisher"
                                    , listCategoriesEnglish.get(4).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Confrontation"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 5.29
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesEnglish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "There for you"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 4.41
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesEnglish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Welcome to Jamrock"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 3.33
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesEnglish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "The Master has come back"
                                    , "https://img.discogs.com/nc4OcqY_nyIJIPtiPUeKMiuCDRc=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-2585585-1299156932.jpeg.jpg"
                                    , 4.41
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesEnglish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "All night"
                                    , "https://m.media-amazon.com/images/I/61RicEE+uZL._SS500_.jpg"
                                    , 3.30
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesEnglish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Beautiful"
                                    , "https://images.genius.com/2c94c28d56c05687338a838954289130.500x500x1.jpg"
                                    , 4.48
                                    , "Damian Marley - [Welcome to Jamrock]"
                                    , "Damian Marley"
                                    , listCategoriesEnglish.get(5).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Battery"
                                    , "https://i.ytimg.com/vi/VDwl2jfv-RU/maxresdefault.jpg"
                                    , 5.10
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesEnglish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Master of Puppets"
                                    , "https://img.cdandlp.com/2017/10/imgL/118972381.jpg"
                                    , 8.38
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesEnglish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "The thing that should not be"
                                    , "https://www.nacionrock.com/wp-content/uploads/metthething.jpg"
                                    , 6.32
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesEnglish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Welcome Home(Sanitarium)"
                                    , "https://img.discogs.com/hIUghWJ0XiXR2uU1e_M5RuskDyU=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-6105580-1430663138-4611.jpeg.jpg"
                                    , 6.28
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesEnglish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Disposable Heroes"
                                    , "https://img.cdandlp.com/2017/10/imgL/118972381.jpg"
                                    , 8.14
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesEnglish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Leper Messiah"
                                    , "https://i.ytimg.com/vi/K1lBA2Ebxx4/hqdefault.jpg"
                                    , 5.38
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesEnglish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Orion"
                                    , "https://i.ytimg.com/vi/cVhkKREAxss/hqdefault.jpg"
                                    , 8.12
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesEnglish.get(6).name
                                    ,1.99                                ,false

                                ),
                                SongEnglish(
                                    "Damage, Inc."
                                    , "https://www.heavymetalmerchant.com/assets/full/RA_TP157.jpg"
                                    , 5.08
                                    , "Metallica - [Master of Puppets]"
                                    , "Metallica"
                                    , listCategoriesEnglish.get(6).name
                                    ,1.99                                ,false

                                )

                            )
                            this.deleteAll()
                            this.insertAll(list)
                        }
                    }
                }
            }
        }

    }
}