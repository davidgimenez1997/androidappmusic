package com.utad.david.androidappmusic.Models.English

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

/*
Se importa Serializable para poder pasar un objeto de esta clase entre pantallas
 */

@Entity(tableName = "albumsEnglish")
data class AlbumEnglish(
    var name: String,
    var image: String,
    var description: String,
    var artist: String,
    var genre: String,
    var songEnglishes: List<SongEnglish>,
    var price:Double,
    var isFavorite:Boolean
):Serializable {
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0

}