package com.utad.david.androidappmusic.DataRoom.Daos.Spanish

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.utad.david.androidappmusic.Models.English.PlaylistEnglish
import com.utad.david.androidappmusic.Models.Spanish.PlaylistSpanish

@Dao
interface DaoPlaylistSpanish{

    /*
    Sentencias sql de nuestra base de datos
     */

    @Insert
    fun insertAll(listPlaylistEnglish:List<PlaylistSpanish>)

    @Query("SELECT * from playlistsSpanish")
    fun getAllPlaylists(): LiveData<List<PlaylistSpanish>>

    @Query("Delete from playlistsSpanish")
    fun deleteAll()

}