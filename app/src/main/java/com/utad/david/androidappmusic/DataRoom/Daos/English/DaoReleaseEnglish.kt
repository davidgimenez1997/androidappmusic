package com.utad.david.androidappmusic.DataRoom.Daos.English

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.utad.david.androidappmusic.Models.English.ReleaseEnglish

@Dao
interface DaoReleaseEnglish{

    /*
    Sentencias sql de nuestra base de datos
     */

    @Insert
    fun insertAll(listReleaseEnglish:List<ReleaseEnglish>)

    @Query("SELECT * from releasesEnglish ")
    fun getAllReleases(): LiveData<List<ReleaseEnglish>>

    @Query("Delete from releasesEnglish")
    fun deleteAll()

}