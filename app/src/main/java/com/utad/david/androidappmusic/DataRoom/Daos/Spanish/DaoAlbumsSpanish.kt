package com.utad.david.androidappmusic.DataRoom.Daos.Spanish

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.utad.david.androidappmusic.Models.English.AlbumEnglish
import com.utad.david.androidappmusic.Models.Spanish.AlbumSpanish

@Dao
interface DaoAlbumsSpanish{

    /*
    Sentencias sql de nuestra base de datos
     */

    @Insert
    fun insertAll(listAlbumEnglish:List<AlbumSpanish>)

    @Query("SELECT * from albumsSpanish ")
    fun getAllAlbums(): LiveData<List<AlbumSpanish>>

    @Query("DELETE from albumsSpanish")
    fun deleteAll()
}