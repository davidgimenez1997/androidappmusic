package com.utad.david.androidappmusic.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.CategoriesEnglish
import com.utad.david.androidappmusic.Models.Spanish.CategoriesSpanish
import com.utad.david.androidappmusic.R

class AdapterCategories() :
    RecyclerView.Adapter<AdapterCategories.CategoriesViewHolder>() {


    interface onItemClickedAdapter{
        fun clickCategorieEnglish(nameCategories: String,type:String)
        fun clickCategorieSpanish(nameCategories: String,type:String)
    }

    private var mCallback : onItemClickedAdapter? = null
    private var type : String? = null

    companion object {
        private var listCategoriesEnglish:List<CategoriesEnglish>? = null
        private var listCategoriesSpanish:List<CategoriesSpanish>? = null
        private lateinit var context: Context

        /*
Diferentes instancias donde las pasas el mutableList dependiendo del idioma con la info
 */
        fun newInstanceCategoriesEnglish(listCategoriesEnglish:List<CategoriesEnglish>?,context: Context):AdapterCategories{
            this.listCategoriesEnglish = listCategoriesEnglish
            this.context = context
            return  AdapterCategories()
        }

        fun newInstanceCategoriesSpanish(listCategoriesSpanish:List<CategoriesSpanish>?,context: Context):AdapterCategories{
            this.listCategoriesSpanish = listCategoriesSpanish
            this.context = context
            return  AdapterCategories()
        }
    }


    fun setCallback(callback: onItemClickedAdapter){
        mCallback = callback
    }

    fun setType(type:String?){
        this.type = type
    }

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): AdapterCategories.CategoriesViewHolder {
        val rootView =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_categories, viewGroup, false)
        return CategoriesViewHolder(rootView)
    }

    override fun onBindViewHolder(categoriesHolderDialog: CategoriesViewHolder, i: Int) {

        if(LocaleHelper().getLanguage(context).equals("en")){
            configCategoriesEnglish(categoriesHolderDialog,i)
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            configCategoriesSpanish(categoriesHolderDialog,i)
        }
    }

    /*
      Configuración de las categorías dependiendo de el idioma y el producto (song/album)
       */
    private fun configCategoriesEnglish(categoriesHolderDialog: CategoriesViewHolder, i: Int){
        var item = listCategoriesEnglish?.get(i);
        categoriesHolderDialog.nameCategories.text = item?.name

        if(type.equals("song")){
            configSongCategoriesEnglish(categoriesHolderDialog,item)
        }else if(type.equals("album")){
            configAlbumCategoriesEnglish(categoriesHolderDialog,item)
        }
    }

    private fun configCategoriesSpanish(categoriesHolderDialog: CategoriesViewHolder, i: Int){
        var item = listCategoriesSpanish?.get(i)
        categoriesHolderDialog.nameCategories.text = item?.name

        if(type.equals("song")){
            configSongCategoriesSpanish(categoriesHolderDialog,item)
        }else if(type.equals("album")){
            configAlbumCategoriesSpanish(categoriesHolderDialog,item)
        }
    }
/*
Ponemos un onClick en la celda de la categoría, pasando por parámetro el nombre y el tipo de la categoría
 */
    private fun configSongCategoriesEnglish(categoriesHolderDialog: CategoriesViewHolder, item: CategoriesEnglish?){
        categoriesHolderDialog.itemView.setOnClickListener{
            if (item != null) {
                mCallback.let {
                    it?.clickCategorieEnglish(item.name,"song");
                }
            }
        }
    }

    private fun configAlbumCategoriesEnglish(categoriesHolderDialog: CategoriesViewHolder, item: CategoriesEnglish?){
        categoriesHolderDialog.itemView.setOnClickListener{
            if (item != null) {
                mCallback.let {
                    it?.clickCategorieEnglish(item.name,"album");
                }
            }
        }
    }

    private fun configSongCategoriesSpanish(categoriesHolderDialog: CategoriesViewHolder, item: CategoriesSpanish?){
        categoriesHolderDialog.itemView.setOnClickListener{
            if (item != null) {
                mCallback.let {
                    it?.clickCategorieSpanish(item.name,"song");
                }
            }
        }
    }

    private fun configAlbumCategoriesSpanish(categoriesHolderDialog: CategoriesViewHolder, item: CategoriesSpanish?){
        categoriesHolderDialog.itemView.setOnClickListener{
            if (item != null) {
                mCallback.let {
                    it?.clickCategorieSpanish(item.name,"album");
                }
            }
        }
    }

    override fun getItemCount(): Int {
        if(LocaleHelper().getLanguage(context).equals("en")){
            listCategoriesEnglish.let {
                if (it != null) {
                    return it.size
                }
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            listCategoriesSpanish.let {
                if(it!=null){
                    return it.size
                }
            }
        }
        return 0
    }

    inner class CategoriesViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        var nameCategories: TextView

        init {
            nameCategories = v.findViewById(R.id.nameCategories)
        }
    }
}
