package com.utad.david.androidappmusic.Fragments


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.utad.david.androidappmusic.R
import android.os.Handler
import android.support.v4.view.ViewPager
import com.utad.david.androidappmusic.Adapter.AdapterSlidingImageEnglish
import com.utad.david.androidappmusic.Adapter.AdapterSlidingImageSpanish
import com.utad.david.androidappmusic.DataRoom.Repositorys.English.RepositoryReleasesEnglish
import com.utad.david.androidappmusic.DataRoom.Repositorys.Spanish.RepositoryReleasesSpanish
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.ReleaseEnglish
import com.utad.david.androidappmusic.Models.Spanish.ReleaseSpanish
import kotlinx.android.synthetic.main.fragment_releases.view.*
import java.util.ArrayList
import java.util.Timer
import java.util.TimerTask

class ReleasesFragment : Fragment() {

    private var myReleasesEnglishList = listOf<ReleaseEnglish>()
    private var releasesEnglishArrayList: ArrayList<ReleaseEnglish>? = null

    private var myReleasesSpanishList = listOf<ReleaseSpanish>()
    private var releasesSpanishArrayList: ArrayList<ReleaseSpanish>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_releases, container, false)

        if(LocaleHelper().getLanguage(view.context).equals("en")){
            configureViewEnglish(view)
        }else if(LocaleHelper().getLanguage(view.context).equals("es")){
            configureViewSpanish(view)
        }

        return view
    }

    /*
    Convertimos la lista de releases de la base de datos en un arrayList y llamamos al los metodos init correspondientes
    segun el idioma
     */

    private fun configureViewEnglish(view: View) {
        activity?.let {
            RepositoryReleasesEnglish.getInstance(it.application).getAllReleases()?.observe(this, Observer {
                it?.let { list ->
                    myReleasesEnglishList = list;
                    releasesEnglishArrayList = ArrayList()
                    releasesEnglishArrayList = populateListEnglish()
                    initEnglish(view)
                }
            })
        }
    }

    private fun configureViewSpanish(view: View) {
        activity?.let {
            RepositoryReleasesSpanish.getInstance(it.application).getAllReleases()?.observe(this, Observer {
                it?.let { list ->
                    myReleasesSpanishList = list;
                    releasesSpanishArrayList = ArrayList()
                    releasesSpanishArrayList = populateListSpanish()
                    initSpanish(view)
                }
            })
        }
    }

    /*
    Devuelven un arraylist con la informacion de la lista
     */

    private fun populateListEnglish(): ArrayList<ReleaseEnglish> {

        val list = ArrayList<ReleaseEnglish>()

        myReleasesEnglishList.forEach({
            list.add(it)
        })

        return list
    }

    private fun populateListSpanish(): ArrayList<ReleaseSpanish> {

        val list = ArrayList<ReleaseSpanish>()

         myReleasesSpanishList.forEach({
            list.add(it)
        })

        return list
    }


    private fun initEnglish(view: View) {

        /*
        Configuramos el adapter del viewPager
         */

        activity?.let { activity ->
            releasesEnglishArrayList?.let {
                view.mPager?.adapter = AdapterSlidingImageEnglish(activity, it)

            }

        }

        //Numero de paginas del viewPager

        releasesEnglishArrayList?.let {
            NUM_PAGES = it.size

        }


        //Configuracion de la animacion del carrusel

        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            view.mPager!!.setCurrentItem(currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 3000, 3000)

        // Pager listener over indicator
        view.indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(position: Int) {
                currentPage = position

            }
        })

    }

    private fun initSpanish(view: View) {

        /*
        Configuramos el adapter del viewPager
         */

        activity?.let { activity ->
            releasesSpanishArrayList?.let {
                view.mPager?.adapter = AdapterSlidingImageSpanish(activity, it)
            }

        }

        //Numero de paginas del viewPager

        releasesSpanishArrayList?.let {
            NUM_PAGES = it.size

        }

        //Configuracion de la animacion del carrusel

        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            view.mPager?.let {
                it.setCurrentItem(currentPage++, true)
            }
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 3000, 3000)

        // Pager listener over indicator
        view.indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position

            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {

            }
        })

    }

    companion object {
        private lateinit var mPager: ViewPager
        private var currentPage = 0
        private var NUM_PAGES = 0
    }

}