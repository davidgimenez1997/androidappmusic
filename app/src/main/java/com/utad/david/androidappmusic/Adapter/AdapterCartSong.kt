package com.utad.david.androidappmusic.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish
import com.utad.david.androidappmusic.R
import kotlinx.android.synthetic.main.item_card.view.*

class AdapterCartSong() : RecyclerView.Adapter<AdapterCartSong.ViewHolderSong>(){

    companion object {
        private var songEnglishList: List<SongEnglish> = listOf()
        private var songSpanishList: List<SongSpanish> = listOf()
        private lateinit var context : Context


        fun newInstanceSongEnglish( songEnglishList: List<SongEnglish>, context: Context): AdapterCartSong {
            this.songEnglishList = songEnglishList;
            this.context = context
            return AdapterCartSong()
        }

        fun newInstanceSongSpanish(songSpanishList: List<SongSpanish>, context: Context): AdapterCartSong{
            this.songSpanishList = songSpanishList
            this.context = context
            return AdapterCartSong()
        }

        fun setSongSpanishList(songSpanishList: List<SongSpanish>){
            this.songSpanishList = songSpanishList
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderSong {
        val rootView = LayoutInflater.from(p0.context).inflate(R.layout.item_card, p0, false)
        return ViewHolderSong(rootView)
    }

    override fun getItemCount(): Int {
        if(LocaleHelper().getLanguage(context).equals("en")){
            songEnglishList?.let {
                return it.size
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            songSpanishList?.let {
                return it.size
            }
        }

        return 0
    }

    override fun onBindViewHolder(myViewHolder: ViewHolderSong, position: Int) {
        viewHolderSong(myViewHolder, position, context)
    }

    private fun viewHolderSong(myViewHolder: ViewHolderSong, position: Int,context: Context){
        if(LocaleHelper().getLanguage(context).equals("en")){
            songEnglishList.let {
                val item = it?.get(position)
                myViewHolder.setValueSongEnglish(item)
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")) {
            songSpanishList.let {
                val item = it?.get(position)
                myViewHolder.setValueSongSpanish(item)
            }
        }
    }

    inner class ViewHolderSong(val view: View): RecyclerView.ViewHolder(view) {
        var title: TextView = itemView.textViewTitlecard
        var photo: ImageView = itemView.imagenviewCard
        var genre : TextView = itemView.textViewGenreCard
        var price :TextView = itemView.textViewPrice


        fun setValueSongEnglish(item: SongEnglish){
            title.setText(item.name)
            genre.setText(item.genre)
            price.text = item.price.toString()
            Glide.with(itemView).load(item.image).into(photo)
        }

        fun setValueSongSpanish(item: SongSpanish){
            title.setText(item.name)
            genre.setText(item.genre)
            price.text = item.price.toString()
            Glide.with(itemView).load(item.image).into(photo)
        }
    }
}