package com.utad.david.androidappmusic.DataRoom.Repositorys.Spanish

import android.app.Application
import android.arch.lifecycle.LiveData
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish
import com.utad.david.androidappmusic.DataRoom.Daos.Spanish.DaoReleaseSpanish
import com.utad.david.androidappmusic.Models.English.ReleaseEnglish
import com.utad.david.androidappmusic.Models.Spanish.ReleaseSpanish

class RepositoryReleasesSpanish private constructor(application: Application) {

    /*
  Creamos una instancia del dao y un livedata para poder observar los datos en diferentes partes de nuestra aplicacion
   */

    private var mDaoReleaseSpanish: DaoReleaseSpanish? = null
    private var mReleases: LiveData<List<ReleaseSpanish>>? = null


    companion object {
        @Volatile
        private var INSTANCE: RepositoryReleasesSpanish? = null

        fun getInstance(context: Application): RepositoryReleasesSpanish =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: RepositoryReleasesSpanish(context).also { INSTANCE = it }
            }
    }

    init {
        BDMusicAppEnglish.getInstance(application).also {
            mDaoReleaseSpanish = it.releaseDaoSpanish()
            mReleases = mDaoReleaseSpanish?.getAllReleases()
        }
    }


    fun getAllReleases(): LiveData<List<ReleaseSpanish>>? {
        return mReleases
    }

}