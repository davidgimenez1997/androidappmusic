package com.utad.david.androidappmusic.Fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.utad.david.androidappmusic.R

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)

        var imageButtonTwitter : ImageButton = view.findViewById(R.id.imageButtonTwitter)
        var imageButtonFacebook : ImageButton = view.findViewById(R.id.imageButtonFacebook)
        var imageButtonInstagram : ImageButton = view.findViewById(R.id.imageButtonInstagram)

        /*
        Metodos onClick de los botones de la pantalla Home
         */

        imageButtonTwitter.setOnClickListener {
            val url = "https://twitter.com/MusicMarketApp1"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }

        imageButtonFacebook.setOnClickListener {
            val url = "https://www.facebook.com/Musicmarketapp-402289467212273/?__tn__=kC-R&eid=ARBSEUfAp6Yc1GcavjCtZOo6Fae_iTsiBERh1k8Qsn2TN4Zj03SheqYTO7xeeK1g81M4j1pE6zrtzksf&hc_ref=ARQR3Xm2C92OH7LlReTeEIP1ib_lP-9CRSCfRXpznb3yCuIXBX4vdwgCMNQw5o90PdM"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }

        imageButtonInstagram.setOnClickListener {
            val url = "https://www.instagram.com/musicmarketapp/"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }

        return view
    }

}
