package com.utad.david.androidappmusic.DataRoom.Daos.Spanish

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.utad.david.androidappmusic.Models.English.ReleaseEnglish
import com.utad.david.androidappmusic.Models.Spanish.ReleaseSpanish

@Dao
interface DaoReleaseSpanish{

    /*
    Sentencias sql de nuestra base de datos
     */

    @Insert
    fun insertAll(listReleaseEnglish:List<ReleaseSpanish>)

    @Query("SELECT * from releasesSpanish ")
    fun getAllReleases(): LiveData<List<ReleaseSpanish>>

    @Query("Delete from releasesSpanish")
    fun deleteAll()

}