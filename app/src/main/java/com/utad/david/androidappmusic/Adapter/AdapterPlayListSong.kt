package com.utad.david.androidappmusic.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish
import com.utad.david.androidappmusic.R
import kotlinx.android.synthetic.main.item_favorite.view.*


class AdapterPlayListSong() : RecyclerView.Adapter<AdapterPlayListSong.ViewHolderFavorite>(){

    interface OnFragmentInteractionListener {
        fun clickCartButtonEnglishSong(songEnglish: SongEnglish)
        fun clickCartButtonSpanishSong(songSpanish: SongSpanish)
    }

    private var mCallback : OnFragmentInteractionListener? = null

    fun setCallback(callback: OnFragmentInteractionListener){
        mCallback = callback
    }

    companion object {

        private var mutableListSongEnglish: MutableList<SongEnglish> = mutableListOf()
        private var mutableListSongSpanish: MutableList<SongSpanish> = mutableListOf()
        private lateinit var context : Context

/*
Diferentes instancias donde las pasas el mutableList dependiendo del idioma con la info
 */
        fun newInstanceSongSpanish(mutableListSongSpanish: MutableList<SongSpanish>,context: Context):AdapterPlayListSong{
            this.context = context
            this.mutableListSongSpanish = mutableListSongSpanish
            return AdapterPlayListSong()
        }

        fun newInstanceSongEnglish(mutableListSongEnglish: MutableList<SongEnglish>,context: Context):AdapterPlayListSong{
            this.context = context
            this.mutableListSongEnglish = mutableListSongEnglish
            return AdapterPlayListSong()
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderFavorite {
        val rootView = LayoutInflater.from(p0.context).inflate(R.layout.item_favorite, p0, false)
        return ViewHolderFavorite(rootView)
    }

    override fun getItemCount(): Int {
        if(LocaleHelper().getLanguage(context).equals("en")){
            mutableListSongEnglish?.let {
                return it.size
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            mutableListSongEnglish?.let {
                return it.size
            }
        }

        return 0
    }

    override fun onBindViewHolder(myViewHolder: ViewHolderFavorite, position: Int) {
        viewHolderAlbum(myViewHolder, position, context)
    }

    private fun viewHolderAlbum(myViewHolder: ViewHolderFavorite, position: Int,context: Context){
        if(LocaleHelper().getLanguage(context).equals("en")){
            if(!mutableListSongEnglish.isEmpty()){
                mutableListSongEnglish?.let {
                    val item = it?.get(position)
                    myViewHolder.setValueSongEnglish(item)
                }
            }
        }else if(LocaleHelper().getLanguage(context).equals("es")){
            if(!mutableListSongSpanish.isEmpty()){
                mutableListSongSpanish?.let {
                    val item = it?.get(position)
                    myViewHolder.setValueSongSpanish(item)
                }
            }
        }
    }

    inner class ViewHolderFavorite(val view: View): RecyclerView.ViewHolder(view) {
        var name: TextView = itemView.textViewtitlefavorite
        var price :TextView = itemView.textViewpricefavorite
        var photo : ImageView = itemView.imageViewFavorite
        var cart :ImageView = itemView.imageViewfavoritecard

        /*
        Ponemos la información del item en la celda
         */

        fun setValueSongEnglish(item: SongEnglish) {
            name.setText(item.name)
            price.text = item.price.toString()
            Glide.with(itemView).load(item.image).into(photo)
            cart.setOnClickListener {
                Toast.makeText(itemView.context,context.getString(R.string.info_button_Song_card)+" " +item?.name,
                    Toast.LENGTH_SHORT).show()
                mCallback?.let {listener->
                    listener.clickCartButtonEnglishSong(item)
                }
            }
        }

        fun setValueSongSpanish(item: SongSpanish) {
            name.setText(item.name)
            price.text = item.price.toString()
            Glide.with(itemView).load(item.image).into(photo)
            cart.setOnClickListener {
                Toast.makeText(itemView.context,context.getString(R.string.info_button_Song_card) +" "+item?.name,
                    Toast.LENGTH_SHORT).show()
                mCallback?.let {listener->
                    listener.clickCartButtonSpanishSong(item)
                }
            }
        }
    }
}