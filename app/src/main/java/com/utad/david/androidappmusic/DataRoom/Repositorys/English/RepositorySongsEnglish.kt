package com.utad.david.androidappmusic.DataRoom.Repositorys.English

import android.app.Application
import android.arch.lifecycle.LiveData
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish
import com.utad.david.androidappmusic.DataRoom.Daos.English.DaoSongsEnglish
import com.utad.david.androidappmusic.Models.English.SongEnglish

class RepositorySongsEnglish private constructor(application: Application) {

    /*
Creamos una instancia del dao y un livedata para poder observar los datos en diferentes partes de nuestra aplicacion
*/

    private var mDaoSongsEnglish: DaoSongsEnglish? = null
    private var mSongs: LiveData<List<SongEnglish>>? = null


    companion object {
        @Volatile
        private var INSTANCE: RepositorySongsEnglish? = null

        fun getInstance(context: Application): RepositorySongsEnglish =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: RepositorySongsEnglish(context).also { INSTANCE = it }
            }
    }

    init {
        BDMusicAppEnglish.getInstance(application).also {
            mDaoSongsEnglish = it.songDaoEnglish()
            mSongs = mDaoSongsEnglish?.getAllSongs()
        }
    }


    fun getAllSongs(): LiveData<List<SongEnglish>>? {
        return mSongs
    }

}