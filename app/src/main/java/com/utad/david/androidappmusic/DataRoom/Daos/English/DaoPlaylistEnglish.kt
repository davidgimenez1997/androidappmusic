package com.utad.david.androidappmusic.DataRoom.Daos.English

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.utad.david.androidappmusic.Models.English.PlaylistEnglish

@Dao
interface DaoPlaylistEnglish{

    /*
    Sentencias sql de nuestra base de datos
     */

    @Insert
    fun insertAll(listPlaylistEnglish:List<PlaylistEnglish>)

    @Query("SELECT * from playlistsEnglish ")
    fun getAllPlaylists(): LiveData<List<PlaylistEnglish>>

    @Query("Delete from playlistsEnglish")
    fun deleteAll()

}