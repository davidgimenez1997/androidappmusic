package com.utad.david.androidappmusic.DialogFragments

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.utad.david.androidappmusic.Adapter.AdapterAlbumDetails
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.AlbumEnglish
import com.utad.david.androidappmusic.Models.Spanish.AlbumSpanish

import com.utad.david.androidappmusic.R

private const val ALBUMENGLISH = "ALBUMENGLISH"
private const val ALBUMSPANISH = "ALBUMSPANISH"

class AlbumDetails : DialogFragment() {

    interface OnFragmentInteractionListener {
        fun clickCartButtonEnglishAlbum(albumEnglish: AlbumEnglish)
        fun clickCartButtonSpanishAlbum(albumSpanish: AlbumSpanish)
        fun clickFavoriteSpanishAlbum(albumSpanish: AlbumSpanish)
        fun clickFavoriteEnglisAlbum(albumEnglish: AlbumEnglish)
    }

    private lateinit var myRecycleView: RecyclerView
    private lateinit var myAdapter: RecyclerView.Adapter<AdapterAlbumDetails.ViewHolderAlbumDetails>
    private lateinit var myLayoutManger: RecyclerView.LayoutManager
    private var listener: AlbumDetails.OnFragmentInteractionListener? = null

    companion object {
        @JvmStatic
        fun newInstanceAlbumEnglish(albumEnglish: AlbumEnglish) =
            AlbumDetails().apply {
                arguments = Bundle().apply {
                    putSerializable(ALBUMENGLISH,albumEnglish)
                }
            }
        fun newInstanceAlbumSpanish(albumSpanish: AlbumSpanish) =
            AlbumDetails().apply {
                arguments = Bundle().apply {
                    putSerializable(ALBUMSPANISH,albumSpanish)
                }
            }
    }

    private var albumEnglish: AlbumEnglish? = null
    private var albumSpanish: AlbumSpanish? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            albumEnglish = it.getSerializable(ALBUMENGLISH) as AlbumEnglish?
            albumSpanish = it.getSerializable(ALBUMSPANISH) as AlbumSpanish?
        }
    }

    private lateinit var closeDialog :ImageView
    private lateinit var addCart :ImageView
    private lateinit var titleAlbum :TextView
    private lateinit var photoAlbum :ImageView
    private lateinit var favorite:ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_album_details, container, false)

        //Pone el fondo transparente
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        findById(view)

        if(LocaleHelper().getLanguage(view.context).equals("en")){

            configureViewEnglish(view)

            /*
           Comprueba si el item esta o no en favoritos y dependiendo de esto pone un icono o otro.
            */

            if(albumEnglish?.isFavorite==false){
                favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp)
            }else{
                favorite.setImageResource(R.drawable.ic_favorite_black_24dp)
            }

        }else if(LocaleHelper().getLanguage(view.context).equals("es")){

            configureViewSpanish(view)

            /*
           Comprueba si el item esta o no en favoritos y dependiendo de esto pone un icono o otro.
            */

            if(albumSpanish?.isFavorite==false){
                favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp)
            }else{
                favorite.setImageResource(R.drawable.ic_favorite_black_24dp)
            }
        }

        /*
       Cierra el dialogo
        */

        closeDialog.setOnClickListener {
            dismiss()
        }

        return view
    }

    fun findById(view: View){
        closeDialog = view.findViewById(R.id.closeImageView)
        addCart = view.findViewById(R.id.addCardImageView)
        titleAlbum = view.findViewById(R.id.textViewTitleAlbum)
        photoAlbum = view.findViewById(R.id.imageDetailsAlbum)
        myRecycleView = view.findViewById(R.id.recyclerViewDetailsAlbum)
        myLayoutManger = GridLayoutManager(view.context,1)
        myRecycleView.layoutManager = myLayoutManger
        favorite = view.findViewById(R.id.imageViewFavoriteAlbum)
    }

    /*
    Configura la vista con la informacion del item
     */

    fun configureViewEnglish(view: View){
        titleAlbum.text = albumEnglish?.name
        Glide.with(view).load(albumEnglish?.image).into(photoAlbum)
        myAdapter = AdapterAlbumDetails.newInstanceAlbumEnglish(albumEnglish?.songEnglishes,view.context)
        myRecycleView.setHasFixedSize(true)
        myRecycleView.adapter = myAdapter
        clickCardButtonEnglish()
        clickFavoriteEnglish()
    }

    /*
   Onclick de los botones del carrito y favoritos
    */

    private fun clickCardButtonEnglish(){
        addCart.setOnClickListener {
            Toast.makeText(this.context,getString(R.string.info_button_Album_card)+" " +albumEnglish?.name,Toast.LENGTH_SHORT).show()
            listener?.let {listener->
                albumEnglish?.let {
                    listener.clickCartButtonEnglishAlbum(it)
                }
            }
            dismiss()
        }
    }

    private fun clickFavoriteEnglish(){
        favorite.setOnClickListener {
            listener?.let {
                albumEnglish?.let {albumEnglish ->
                    it.clickFavoriteEnglisAlbum(albumEnglish)
                    albumEnglish.isFavorite = true
                    favorite.setImageResource(R.drawable.ic_favorite_black_24dp)
                }
            }
        }

    }

    /*
    Configura la vista con la informacion del item
     */

    fun configureViewSpanish(view: View){
        titleAlbum.text = albumSpanish?.name
        Glide.with(view).load(albumSpanish?.image).into(photoAlbum)
        myAdapter = AdapterAlbumDetails.newInstanceAlbumSpanish(albumSpanish?.songEnglishes,view.context)
        myRecycleView.adapter = myAdapter
        clickCardButtonSpanish()
        clickFavoriteSpanish()
    }

    /*
   Onclick de los botones del carrito y favoritos
    */

    private fun clickCardButtonSpanish(){
        addCart.setOnClickListener {
            Toast.makeText(this.context,getString(R.string.info_button_Album_card)+" " +albumSpanish?.name,Toast.LENGTH_SHORT).show()
            listener?.let {listener->
                albumSpanish?.let {
                    listener.clickCartButtonSpanishAlbum(it)
                }
            }
            dismiss()
        }
    }

    private fun clickFavoriteSpanish(){
        favorite.setOnClickListener {
            listener?.let {
                albumSpanish?.let { albumSpanish ->
                    it.clickFavoriteSpanishAlbum(albumSpanish)
                    albumSpanish.isFavorite = true
                    favorite.setImageResource(R.drawable.ic_favorite_black_24dp)
                }
            }
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}
