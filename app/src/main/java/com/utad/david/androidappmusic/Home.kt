package com.utad.david.androidappmusic

import android.app.Application
import android.content.Context
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish

class Home: Application() {


    //Consigue que el idioma por defecto sea el ingles

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleHelper().onAttach(base,"en"))
    }
}