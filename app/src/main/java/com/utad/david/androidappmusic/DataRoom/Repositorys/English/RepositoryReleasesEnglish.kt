package com.utad.david.androidappmusic.DataRoom.Repositorys.English

import android.app.Application
import android.arch.lifecycle.LiveData
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish
import com.utad.david.androidappmusic.DataRoom.Daos.English.DaoReleaseEnglish
import com.utad.david.androidappmusic.Models.English.ReleaseEnglish

class RepositoryReleasesEnglish private constructor(application: Application) {

    /*
Creamos una instancia del dao y un livedata para poder observar los datos en diferentes partes de nuestra aplicacion
*/

    private var mDaoReleasesEnglish: DaoReleaseEnglish? = null
    private var mReleases: LiveData<List<ReleaseEnglish>>? = null


    companion object {
        @Volatile
        private var INSTANCE: RepositoryReleasesEnglish? = null

        fun getInstance(context: Application): RepositoryReleasesEnglish =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: RepositoryReleasesEnglish(context).also { INSTANCE = it }
            }
    }

    init {
        BDMusicAppEnglish.getInstance(application).also {
            mDaoReleasesEnglish = it.releaseDaoEnglish()
            mReleases = mDaoReleasesEnglish?.getAllReleases()
        }
    }


    fun getAllReleases(): LiveData<List<ReleaseEnglish>>? {
        return mReleases
    }

}