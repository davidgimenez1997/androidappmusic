package com.utad.david.androidappmusic.Fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.utad.david.androidappmusic.Adapter.AdapterAlbumSong
import com.utad.david.androidappmusic.Adapter.AdapterCategories
import com.utad.david.androidappmusic.Adapter.AdapterPlayListAlbum
import com.utad.david.androidappmusic.Adapter.AdapterPlayListSong
import com.utad.david.androidappmusic.DialogFragments.CartFragment
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.AlbumEnglish
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.AlbumSpanish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish

import com.utad.david.androidappmusic.R

class PlaylistFragment : Fragment(),AdapterPlayListAlbum.OnFragmentInteractionListener,AdapterPlayListSong.OnFragmentInteractionListener{

    companion object {

        /*
        Variables y Metodos para settear los datos a las mutablelist
         */

        private var mutableListAlbumEnglish: MutableList<AlbumEnglish> = mutableListOf()
        private var mutableListAlbumSpanish: MutableList<AlbumSpanish> = mutableListOf()
        private var mutableListSongEnglish: MutableList<SongEnglish> = mutableListOf()
        private var mutableListSongSpanish: MutableList<SongSpanish> = mutableListOf()

        fun setMutableListAlbumEnglish(mutableListAlbumEnglish: MutableList<AlbumEnglish>){
            this.mutableListAlbumEnglish = mutableListAlbumEnglish
        }

        fun setMutableListAlbumSpanish(mutableListAlbumSpanish: MutableList<AlbumSpanish>){
            this.mutableListAlbumSpanish = mutableListAlbumSpanish
        }

        fun setMutableListSongEnglish(mutableListSongEnglish: MutableList<SongEnglish>){
            this.mutableListSongEnglish = mutableListSongEnglish
        }

        fun setMutableListSongSpanish(mutableListSongSpanish: MutableList<SongSpanish>){
            this.mutableListSongSpanish = mutableListSongSpanish
        }
    }

    //Variables

    private lateinit var mRecycleViewSong: RecyclerView
    private lateinit var mRecycleViewAlbum: RecyclerView

    private lateinit var myAdapterSong: RecyclerView.Adapter<AdapterPlayListSong.ViewHolderFavorite>
    private lateinit var myAdapterAlbum: RecyclerView.Adapter<AdapterPlayListAlbum.ViewHolderFavorite>

    private lateinit var myLayoutMangerSong: RecyclerView.LayoutManager
    private lateinit var myLayoutMangerAlbum: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_playlist, container, false)

        findById(view)
        configView(view)

        return view
    }

    private fun findById(view: View){
        mRecycleViewAlbum = view.findViewById(R.id.recycleviewfavoritealbum)
        mRecycleViewSong = view.findViewById(R.id.recycleviewfavoritesong)
        mRecycleViewAlbum.setHasFixedSize(true)
        mRecycleViewSong.setHasFixedSize(true)
    }

    private fun configView(view: View){

        if(LocaleHelper().getLanguage(view.context).equals("en")){

            configAlbumEnglish(view)
            configSongEnglish(view)

        }else if(LocaleHelper().getLanguage(view.context).equals("es")){

            configAlbumSpanish(view)
            configSongSpanish(view)

        }
    }

    /*
    Creamos el adapter segun el tipo y el idioma correspondiente, pasandole por parametro la mutablelist rellena con los objetos favoritos.
    Se le aplica un callback para el boton del carrito.
     */

    private fun configAlbumEnglish(view: View){
        myAdapterAlbum = AdapterPlayListAlbum.newInstanceAlbumEnglish(mutableListAlbumEnglish,view.context).apply {
            setCallback(this@PlaylistFragment)
        }
        myLayoutMangerAlbum = GridLayoutManager(view.context, 1)
        mRecycleViewAlbum.layoutManager = myLayoutMangerAlbum
        mRecycleViewAlbum.adapter = myAdapterAlbum
    }

    private fun configAlbumSpanish(view: View){
        myAdapterAlbum = AdapterPlayListAlbum.newInstanceAlbumSpanish(mutableListAlbumSpanish,view.context).apply {
            setCallback(this@PlaylistFragment)
        }
        myLayoutMangerAlbum = GridLayoutManager(view.context, 1)
        mRecycleViewAlbum.layoutManager = myLayoutMangerAlbum

        mRecycleViewAlbum.adapter = myAdapterAlbum
    }

    private fun configSongEnglish(view: View){
        myAdapterSong = AdapterPlayListSong.newInstanceSongEnglish(mutableListSongEnglish,view.context).apply {
            setCallback(this@PlaylistFragment)
        }
        myLayoutMangerSong = GridLayoutManager(view.context, 1)
        mRecycleViewSong.layoutManager = myLayoutMangerSong
        mRecycleViewSong.adapter = myAdapterSong
    }

    private fun configSongSpanish(view: View){
        myAdapterSong = AdapterPlayListSong.newInstanceSongSpanish(mutableListSongSpanish,view.context).apply {
            setCallback(this@PlaylistFragment)
        }

        myLayoutMangerSong = GridLayoutManager(view.context, 1)

        mRecycleViewSong.layoutManager = myLayoutMangerSong
        mRecycleViewSong.adapter = myAdapterSong
    }

    private var listAuxAlbumEnglish: List<AlbumEnglish> = listOf()
    private var listAuxSongEnglish: List<SongEnglish> = listOf()
    private var listAuxAlbumSpanish: List<AlbumSpanish> = listOf()
    private var listAuxSongSpanish: List<SongSpanish> = listOf()

    /*
    Callbacks del boton carrito de la celda de favoritos. En estos metodos se añade el item a la lista correspondiente y se llama al setter correspondiente
     */

    override fun clickCartButtonEnglishAlbum(albumEnglish: AlbumEnglish) {
        listAuxAlbumEnglish += albumEnglish
        CartFragment.apply {
            setListAlbumEnglish(listAuxAlbumEnglish)
        }
    }

    override fun clickCartButtonSpanishAlbum(albumSpanish: AlbumSpanish) {
        listAuxAlbumSpanish += albumSpanish
        CartFragment.apply {
            setListAlbumSpanish(listAuxAlbumSpanish)
        }
    }

    override fun clickCartButtonEnglishSong(songEnglish: SongEnglish) {
        listAuxSongEnglish += songEnglish
        CartFragment.apply {
            setListSongEnglish(listAuxSongEnglish)
        }
    }

    override fun clickCartButtonSpanishSong(songSpanish: SongSpanish) {
        listAuxSongSpanish += songSpanish
        CartFragment.apply {
            setListSongSpanish(listAuxSongSpanish)
        }
    }
}
