package com.utad.david.androidappmusic.DataRoom.Daos.English

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.utad.david.androidappmusic.Models.English.AlbumEnglish

@Dao
interface DaoAlbumsEnglish{

    /*
    Sentencias sql de nuestra base de datos
     */

    @Insert
    fun insertAll(listAlbumEnglish:List<AlbumEnglish>)

    @Query("SELECT * from albumsEnglish ")
    fun getAllAlbums(): LiveData<List<AlbumEnglish>>

    @Query("DELETE from albumsEnglish")
    fun deleteAll()
}