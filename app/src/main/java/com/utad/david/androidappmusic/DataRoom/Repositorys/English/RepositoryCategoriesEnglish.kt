package com.utad.david.androidappmusic.DataRoom.Repositorys.English

import android.app.Application
import android.arch.lifecycle.LiveData
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish
import com.utad.david.androidappmusic.DataRoom.Daos.English.DaoCategoriesEnglish
import com.utad.david.androidappmusic.Models.English.CategoriesEnglish

class RepositoryCategoriesEnglish private constructor(application: Application) {

    /*
Creamos una instancia del dao y un livedata para poder observar los datos en diferentes partes de nuestra aplicacion
*/

    private var mDaoCategoriesEnglish: DaoCategoriesEnglish? = null
    private var mCategoriesEnglish: LiveData<List<CategoriesEnglish>>? = null


    companion object {
        @Volatile
        private var INSTANCE: RepositoryCategoriesEnglish? = null

        fun getInstance(context: Application): RepositoryCategoriesEnglish =
            INSTANCE
                ?: synchronized(this) {
                INSTANCE
                    ?: RepositoryCategoriesEnglish(context).also { INSTANCE = it }
            }
    }

    init {
        BDMusicAppEnglish.getInstance(application).also {
            mDaoCategoriesEnglish = it.categoryDaoEnglish()
            mCategoriesEnglish = mDaoCategoriesEnglish?.getAllCategories()
        }
    }


    fun getAllCategories(): LiveData<List<CategoriesEnglish>>? {
        return mCategoriesEnglish
    }

}