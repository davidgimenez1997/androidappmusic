package com.utad.david.androidappmusic.Models.Spanish

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "categoriesSpanish")
data class CategoriesSpanish(
    var name: String
) {
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
}