package com.utad.david.androidappmusic.Models.English

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "categoriesEnglish")
data class CategoriesEnglish(
    var name: String
) {
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
}