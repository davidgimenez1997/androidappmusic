package com.utad.david.androidappmusic.DialogFragments

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish

import com.utad.david.androidappmusic.R

private const val SONGENGLISH = "SONGENGLISH"
private const val SONGSPANISH = "SONGSPANISH"

class SongDetails : DialogFragment() {

    interface OnFragmentInteractionListener {
        fun clickCartButtonEnglishSong(songEnglish: SongEnglish)
        fun clickCartButtonSpanishSong(songSpanish: SongSpanish)
        fun clickFavoriteSpanishSong(songSpanish: SongSpanish)
        fun clickFavoriteEnglisSong(songEnglish: SongEnglish)
    }

    /*
    Pasamos por los metodos "newInstance" el objeto selecionado, diferenciando el idioma
     */

    companion object {

        fun newInstanceSongEnglish(songEnglish: SongEnglish) =
            SongDetails().apply {
                arguments = Bundle().apply {
                    putSerializable(SONGENGLISH, songEnglish)
                }
            }

        fun newInstanceSongsSpanish(songSpanish: SongSpanish) =
            SongDetails().apply {
                arguments = Bundle().apply {
                    putSerializable(SONGSPANISH,songSpanish)
                }
            }
    }

    private var songEnglish: SongEnglish? = null
    private var songSpanish: SongSpanish? = null
    private var listener: SongDetails.OnFragmentInteractionListener? = null

    /*
    Usamos los argumentos del fragment para iniciar nuestros objetos
     */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            songEnglish = it.getSerializable(SONGENGLISH) as SongEnglish?
            songSpanish = it.getSerializable(SONGSPANISH) as SongSpanish?
        }
    }

    private lateinit var closeDialog :ImageView
    private lateinit var addCart:ImageView
    private lateinit var titleAlbum :TextView
    private lateinit var photoAlbum :ImageView
    private lateinit var categoryAlbum: TextView
    private lateinit var durationAlbum: TextView
    private lateinit var favorite:ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_song_details, container, false)

        //Pone el fondo transparente
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        findById(view)

        if(LocaleHelper().getLanguage(view.context).equals("en")){

            configureViewEnglish(view)

            /*
            Comprueba si el item esta o no en favoritos y dependiendo de esto pone un icono o otro.
             */

            if(songEnglish?.isFavorite==false){
                favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp)
            }else{
                favorite.setImageResource(R.drawable.ic_favorite_black_24dp)
            }

        }else if(LocaleHelper().getLanguage(view.context).equals("es")){

           configureViewSpanish(view)

            /*
           Comprueba si el item esta o no en favoritos y dependiendo de esto pone un icono o otro.
            */

            if(songSpanish?.isFavorite==false){
                favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp)
            }else{
                favorite.setImageResource(R.drawable.ic_favorite_black_24dp)
            }
        }

        /*
        Cierra el dialogo
         */

        closeDialog.setOnClickListener {
            dismiss()
        }

        return view
    }

    fun findById(view: View){
        closeDialog = view.findViewById(R.id.closeImageView)
        addCart = view.findViewById(R.id.addCardImageView)
        titleAlbum = view.findViewById(R.id.textViewTitleAlbum)
        photoAlbum = view.findViewById(R.id.imageDetailsAlbum)
        categoryAlbum = view.findViewById(R.id.txtCategory)
        durationAlbum = view.findViewById(R.id.txtDuration)
        favorite = view.findViewById(R.id.imageViewFavoriteSong)
    }

    /*
    Configura la vista con la informacion del item
     */

    fun configureViewEnglish(view: View){
        titleAlbum.text = songEnglish?.name
        categoryAlbum.text = songEnglish?.genre
        durationAlbum.text = songEnglish?.duration.toString()
        Glide.with(view).load(songEnglish?.image).into(photoAlbum)
        clickCartButtonEnglish()
        clickFavoriteEnglish()
    }

    /*
    Onclick de los botones del carrito y favoritos
     */

    private fun clickCartButtonEnglish(){
        addCart.setOnClickListener {
            Toast.makeText(this.context,getString(R.string.info_button_Song_card)+" " +songEnglish?.name, Toast.LENGTH_SHORT).show()
            listener?.let {listener->
                songEnglish?.let {
                    listener.clickCartButtonEnglishSong(it)
                }
            }
            dismiss()
        }
    }

    private fun clickFavoriteEnglish(){
        favorite.setOnClickListener {
            listener?.let {
                songEnglish?.let {songEnglish ->
                    it.clickFavoriteEnglisSong(songEnglish)
                    songEnglish.isFavorite = true
                    favorite.setImageResource(R.drawable.ic_favorite_black_24dp)
                }
            }
        }
    }

    /*
   Configura la vista con la informacion del item
    */

    fun configureViewSpanish(view: View){
        titleAlbum.text = songSpanish?.name
        categoryAlbum.text = songSpanish?.genre
        durationAlbum.text = songSpanish?.duration.toString()
        Glide.with(view).load(songSpanish?.image).into(photoAlbum)
        clickCartButtonSpanish()
        clickFavoriteSpanish()
    }

    /*
    Onclick de los botones del carrito y favoritos
     */

    private fun clickCartButtonSpanish(){
        addCart.setOnClickListener {
            Toast.makeText(this.context,getString(R.string.info_button_Song_card)+" " +songSpanish?.name,Toast.LENGTH_SHORT).show()
            listener?.let {listener->
                songSpanish?.let {
                    listener.clickCartButtonSpanishSong(it)
                }
            }
            dismiss()
        }
    }

    private fun clickFavoriteSpanish(){
        favorite.setOnClickListener {
            listener?.let {
                songSpanish?.let {songSpanish->
                    it.clickFavoriteSpanishSong(songSpanish)
                    songSpanish.isFavorite = true
                    favorite.setImageResource(R.drawable.ic_favorite_black_24dp)
                }
            }
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SongDetails.OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}