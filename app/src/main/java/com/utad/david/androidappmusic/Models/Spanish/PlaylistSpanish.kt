package com.utad.david.androidappmusic.Models.Spanish

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "playlistsSpanish")
class PlaylistSpanish(
    var name: String,
    var image: Int,
    var description: String,
    var artist: String,
    var songEnglishes: List<SongSpanish>
) {
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
}