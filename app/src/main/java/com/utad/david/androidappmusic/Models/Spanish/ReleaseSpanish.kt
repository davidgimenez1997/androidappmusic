package com.utad.david.androidappmusic.Models.Spanish

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "releasesSpanish")
class ReleaseSpanish(
    var image: String,
    var title:String

) {
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
}