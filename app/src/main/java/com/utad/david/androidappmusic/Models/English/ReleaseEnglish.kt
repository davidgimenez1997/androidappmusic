package com.utad.david.androidappmusic.Models.English

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "releasesEnglish")
class ReleaseEnglish(
    var image: String,
    var title:String

) {
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
}