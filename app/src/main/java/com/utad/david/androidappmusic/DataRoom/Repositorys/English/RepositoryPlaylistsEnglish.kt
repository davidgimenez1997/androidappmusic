package com.utad.david.androidappmusic.DataRoom.Repositorys.English

import android.app.Application
import android.arch.lifecycle.LiveData
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish
import com.utad.david.androidappmusic.DataRoom.Daos.English.DaoPlaylistEnglish
import com.utad.david.androidappmusic.Models.English.PlaylistEnglish

class RepositoryPlaylistsEnglish private constructor(application: Application) {

    /*
Creamos una instancia del dao y un livedata para poder observar los datos en diferentes partes de nuestra aplicacion
*/

    private var mDaoPlaylistsEnglish: DaoPlaylistEnglish? = null
    private var mPlaylists: LiveData<List<PlaylistEnglish>>? = null


    companion object {
        @Volatile
        private var INSTANCE: RepositoryPlaylistsEnglish? = null

        fun getInstance(context: Application): RepositoryPlaylistsEnglish =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: RepositoryPlaylistsEnglish(context).also { INSTANCE = it }
            }
    }

    init {
        BDMusicAppEnglish.getInstance(application).also {
            mDaoPlaylistsEnglish = it.playlistDaoEnglish()
            mPlaylists = mDaoPlaylistsEnglish?.getAllPlaylists()
        }
    }


    fun getAllPlaylists(): LiveData<List<PlaylistEnglish>>? {
        return mPlaylists
    }

}