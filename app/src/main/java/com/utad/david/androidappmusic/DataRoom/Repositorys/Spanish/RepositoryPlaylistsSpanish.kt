package com.utad.david.androidappmusic.DataRoom.Repositorys.Spanish

import android.app.Application
import android.arch.lifecycle.LiveData
import com.utad.david.androidappmusic.DataRoom.BDMusicAppEnglish
import com.utad.david.androidappmusic.DataRoom.Daos.Spanish.DaoPlaylistSpanish
import com.utad.david.androidappmusic.Models.English.PlaylistEnglish
import com.utad.david.androidappmusic.Models.Spanish.PlaylistSpanish

class RepositoryPlaylistsSpanish private constructor(application: Application) {

    /*
Creamos una instancia del dao y un livedata para poder observar los datos en diferentes partes de nuestra aplicacion
*/

    private var mDaoPlaylistSpanish: DaoPlaylistSpanish? = null
    private var mPlaylists: LiveData<List<PlaylistSpanish>>? = null


    companion object {
        @Volatile
        private var INSTANCE: RepositoryPlaylistsSpanish? = null

        fun getInstance(context: Application): RepositoryPlaylistsSpanish =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: RepositoryPlaylistsSpanish(context).also { INSTANCE = it }
            }
    }

    init {
        BDMusicAppEnglish.getInstance(application).also {
            mDaoPlaylistSpanish = it.playlistDaoSpanish()
            mPlaylists = mDaoPlaylistSpanish?.getAllPlaylists()
        }
    }


    fun getAllPlaylists(): LiveData<List<PlaylistSpanish>>? {
        return mPlaylists
    }

}