package com.utad.david.androidappmusic.DataRoom.Daos.English

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.utad.david.androidappmusic.Models.English.SongEnglish

@Dao
interface DaoSongsEnglish{

    /*
    Sentencias sql de nuestra base de datos
     */

    @Insert
    fun insertAll(listSongEnglish:List<SongEnglish>)

    @Query("SELECT * from SONGSENGLISH ")
    fun getAllSongs(): LiveData<List<SongEnglish>>

    @Query("Delete from SONGSENGLISH")
    fun deleteAll()
}