package com.utad.david.androidappmusic.DialogFragments

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.utad.david.androidappmusic.Adapter.AdapterCartAlbum
import com.utad.david.androidappmusic.Adapter.AdapterCartSong
import com.utad.david.androidappmusic.LocaleHelper
import com.utad.david.androidappmusic.Models.English.AlbumEnglish
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.AlbumSpanish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish

import com.utad.david.androidappmusic.R

class CartFragment : DialogFragment() {

    private lateinit var myRecycleViewSong: RecyclerView
    private lateinit var mRecycleViewAlbum: RecyclerView
    private lateinit var myAdapterSong: RecyclerView.Adapter<AdapterCartSong.ViewHolderSong>
    private lateinit var myAdapterAlbum: RecyclerView.Adapter<AdapterCartAlbum.ViewHolderAlbum>
    private lateinit var myLayoutSong: RecyclerView.LayoutManager
    private lateinit var myLayoutAlbum: RecyclerView.LayoutManager
    private lateinit var valueCart: TextView
    private lateinit var buttonBuy: Button
    private lateinit var buttonClose : ImageView

    companion object {

        private var listSongEnglish: List<SongEnglish> = listOf()
        private var listSongSpanish: List<SongSpanish> = listOf()
        private var listAlbumEnglish: List<AlbumEnglish> = listOf()
        private var listAlbumSpanish: List<AlbumSpanish> = listOf()

        fun newInstanceCartEnglish(): CartFragment {
            return CartFragment()
        }

        fun newInstanceCartSpanish(): CartFragment {
            return CartFragment()
        }

        /*
        Metodos setters
         */

        fun setListSongSpanish(list: List<SongSpanish>) {
            this.listSongSpanish = list
        }

        fun setListSongEnglish(list: List<SongEnglish>) {
            this.listSongEnglish = list

        }

        fun setListAlbumSpanish(list: List<AlbumSpanish>) {
            this.listAlbumSpanish = list
        }

        fun setListAlbumEnglish(list: List<AlbumEnglish>) {
            this.listAlbumEnglish = list
        }
    }

    private var priceAll: Double = 0.0
    private var priceAllEnhlish = 0.0
    private var priceAllSpanish = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_cart, container, false)

        //Pone el fondo transparente
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        findById(view)
        configRecycleViewAlbum(view)
        configRecycleViewSong(view)

        /*
        Identifica el idioma y lo configura dependiendo de este
         */

        if (LocaleHelper().getLanguage(view.context).equals("en")) {

            configAdapterAlbumEnglish(view)
            configAdapterSongEnglish(view)
            priceAllEnhlish = checkPriceEnglish()
            putPriceEnglish(priceAllEnhlish)
            clickButtonBuyEnglish()


        } else if (LocaleHelper().getLanguage(view.context).equals("es")) {

            configAdapterAlbumSpanish(view)
            configAdapterSongSpanish(view)
            priceAllSpanish = checkPriceSpanish()
            putPriceSpanish(priceAllSpanish)
            clickButtonBuySpanish()

        }

        clickButtonClose()

        return view
    }

    private fun findById(view: View) {
        myRecycleViewSong = view.findViewById(R.id.recyclerViewCardSong)
        mRecycleViewAlbum = view.findViewById(R.id.recyclerViewCardAlbum)
        valueCart = view.findViewById(R.id.textViewValue)
        buttonBuy = view.findViewById(R.id.buttonBuy)
        buttonClose = view.findViewById(R.id.imageViewClose)
    }

    private fun configRecycleViewAlbum(view: View) {
        mRecycleViewAlbum.setHasFixedSize(true)
        myLayoutAlbum = GridLayoutManager(view.context, 1)
        mRecycleViewAlbum.layoutManager = myLayoutAlbum
    }

    private fun configRecycleViewSong(view: View) {
        myRecycleViewSong.setHasFixedSize(true)
        myLayoutSong = GridLayoutManager(view.context, 1)
        myRecycleViewSong.layoutManager = myLayoutSong
    }

    private fun configAdapterSongEnglish(view: View) {
        myAdapterSong = AdapterCartSong.newInstanceSongEnglish(listSongEnglish, view.context)
        myRecycleViewSong.adapter = myAdapterSong
    }

    private fun configAdapterSongSpanish(view: View) {
        myAdapterSong = AdapterCartSong.newInstanceSongSpanish(listSongSpanish, view.context)
        myRecycleViewSong.adapter = myAdapterSong
    }

    private fun configAdapterAlbumEnglish(view: View) {
        myAdapterAlbum = AdapterCartAlbum.newInstanceAlbumEnglish(listAlbumEnglish, view.context)
        mRecycleViewAlbum.adapter = myAdapterAlbum
    }

    private fun configAdapterAlbumSpanish(view: View) {
        myAdapterAlbum = AdapterCartAlbum.newInstanceAlbumSpanish(listAlbumSpanish, view.context)
        mRecycleViewAlbum.adapter = myAdapterAlbum
    }

    /*
        Metodos que devuelven en formato double el precio acumulado en el carrito dependiendo del idioma
     */

    private fun checkPriceEnglish(): Double {
        if (listAlbumEnglish.size != 0 || listSongEnglish.size != 0) {
            for (listAlbumEnglish in listAlbumEnglish) {
                priceAll += listAlbumEnglish.price
            }
            for (listSongEnglish in listSongEnglish) {
                priceAll += listSongEnglish.price
            }
        }
        return priceAll
    }

    private fun checkPriceSpanish(): Double {
        if (listAlbumSpanish.size != 0 || listSongSpanish.size != 0) {
            for (listSongSpanish in listSongSpanish) {
                priceAll += listSongSpanish.price
            }
            for (listAlbumSpanish in listAlbumSpanish) {
                priceAll += listAlbumSpanish.price
            }
        }
        return priceAll
    }

    /*
       Pone el valor por parametro en el textview
     */

    private fun putPriceEnglish(value: Double) {
        valueCart.text = value.toString()
    }

    private fun putPriceSpanish(value: Double) {
        valueCart.text = value.toString()
    }

    /*
    Cierra el dialogo
     */

    private fun clickButtonClose(){
        buttonClose.setOnClickListener {
            dismiss()
        }
    }

    /*
    Pone las listas vacias, llama de nuevo a los metodos de configuracion de los adapters, pone el valor del precio en 0 dependiendo del idioma.
     */

    private fun clickButtonBuyEnglish(){
        buttonBuy.setOnClickListener {
            listSongEnglish = listOf()
            listAlbumEnglish = listOf()
            configAdapterSongEnglish(it)
            configAdapterAlbumEnglish(it)
            putPriceEnglish(0.0)
            Toast.makeText(it.context, getString(R.string.info_buy), Toast.LENGTH_LONG).show()
        }
    }

    private fun clickButtonBuySpanish(){
        buttonBuy.setOnClickListener {
            listSongSpanish = listOf()
            listAlbumSpanish = listOf()
            configAdapterSongSpanish(it)
            configAdapterAlbumSpanish(it)
            putPriceSpanish(0.0)
            Toast.makeText(it.context, getString(R.string.info_buy), Toast.LENGTH_LONG).show()
        }
    }
}
