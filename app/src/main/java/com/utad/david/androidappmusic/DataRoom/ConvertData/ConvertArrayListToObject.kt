package com.utad.david.androidappmusic.DataRoom.ConvertData

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.utad.david.androidappmusic.Models.English.SongEnglish
import com.utad.david.androidappmusic.Models.Spanish.SongSpanish

object ConvertArrayListToObject {

    /*
    Metodos que convierten una lista a String y viceversa de diferentes datos.
     */

    @JvmStatic
    @TypeConverter
    fun convertToArrayEnglish(value: String): List<SongEnglish> {
        return Gson().fromJson(value, Array<SongEnglish>::class.java).toList()
    }

    @JvmStatic
    @TypeConverter
    fun convertToStringEnglish(list: List<SongEnglish>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @JvmStatic
    @TypeConverter
    fun convertToArraySpanish(value: String): List<SongSpanish> {
        return Gson().fromJson(value, Array<SongSpanish>::class.java).toList()
    }

    @JvmStatic
    @TypeConverter
    fun convertToStringSpanish(list: List<SongSpanish>): String {
        val gson = Gson()
        return gson.toJson(list)
    }





}