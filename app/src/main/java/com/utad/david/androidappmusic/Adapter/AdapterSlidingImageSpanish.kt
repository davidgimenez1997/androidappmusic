package com.utad.david.androidappmusic.Adapter

import android.content.Context
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.utad.david.androidappmusic.Models.English.ReleaseEnglish
import com.utad.david.androidappmusic.Models.Spanish.ReleaseSpanish
import com.utad.david.androidappmusic.R
import java.util.ArrayList


/*
Adapter del sliding image, crea la información basándose en el Modelo y pasándole un arraylist por parámetro con la información que se va a mostrar.
*/

class AdapterSlidingImageSpanish(private val context: Context, private val imageModelArrayList: ArrayList<ReleaseSpanish>) : PagerAdapter() {


    private val inflater: LayoutInflater


    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return imageModelArrayList.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false)!!

        val imageView = imageLayout.findViewById(R.id.img_carousel) as ImageView
        val title = imageLayout.findViewById(R.id.texcarusel) as TextView

        // Utilizamos glide para obtener las imagenes  y ponerlas en el imageView

        Glide.with(imageLayout).load(imageModelArrayList[position].image).into(imageView)
        title.text = imageModelArrayList[position].title

        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }



}